addpath common;
addpath dynamic_model;
addpath world_model;
addpath motion_planner;
addpath render;

dynamic_parameters;
kinematic_parameters;
planner_parameters;

%goal: point desidered to reach [xe, ye, vxe, xye]
goals = [[2;0;0;0],[2;2;0;0],[0;2;0;0],[0;0;0;0]];

iteration = 1;

%model of the real robot
real_robot = DynamicModel();

%state of the world == obstacles
world = WorldModel();
world_state = [];
world.UpdateState(world_state);

goal_reached = 1;
goal = goals(:, goal_reached);

planner = Planner(real_robot.GetState(), world, delta_t_max, delta_t_min, ...
                     h, N, we, wu, weev, wv, ws, w_cclear, cclear_min, pruning);
planner.SetGoal(goal);
EE_err = Inf;

%stats for plots
tic_toc = [];
input_traj = {};
q_traj = {};
q_d_traj = {};
v_traj = {};
EEv_traj = {};
time_traj = {};
distance_goal = [];
distance_goal_it = [];
distance_obs = [];
distance_obs_it = [];
trajectory = {};
base_trajectory= {};
time = 0;

while goal_reached <= size(goals, 2)

    fprintf('Iteration: %i--------------------------------\n', iteration);
    elapse_time = tic();
    [U_traj, t_traj] = planner.ControlTrajectory();
    time_to_decision = toc(elapse_time);
    tic_toc(end+1) = time_to_decision;
    distance_obs_it(end+1) = planner.GetCircleClearance;
    
    direct_kin = real_robot.DirectKinematic(real_robot.q);
    EE_err = norm(direct_kin(1:2) - goal(1:2));
    distance_goal_it(end+1) = EE_err;
    if planner.ID_best ~= 1
        if plot_flag
            render = Render(real_robot, world, goal(1:2));
            hold on;
            render.Draw();
            if plot_tree
                render.DrawTree(planner.tree, planner.unpruned, planner.ID_best, planner.v_model);
            end
            drawnow();
            %pause(1);
        end
        %path found apply first k control step
        for i = 1:k
            if (i > size(U_traj, 2)); break, end
            
            real_robot.Step(U_traj(:, i), t_traj(i));
            %if plot_flag,render.Draw(); end
            
            direct_kin = real_robot.DirectKinematic(real_robot.q);
            diff_direct_kin = real_robot.DifferentialDirectKinematic(real_robot.q, real_robot.v);
            time = time + t_traj(i);
             
            EE_err = norm(direct_kin(1:2) - goal(1:2));
            velocity_err = norm(diff_direct_kin(1:2)- goal(3:4));
            fprintf('end effector error: %.3f velocity error: %.3f\n', EE_err, velocity_err);
            input_traj{end+1} = U_traj(:,i);
            q_traj{end+1} = real_robot.q;
            q_d_traj{end+1} = real_robot.q_d;
            v_traj{end+1} = real_robot.v;
            EEv_traj{end+1} = diff_direct_kin(1:2);
            time_traj{end+1} = time;
            trajectory{end+1} = direct_kin(1:2);
            base_trajectory{end+1} = direct_kin(3:4) - lg * [cos(real_robot.q(3)); sin(real_robot.q(3))];
            distance_goal(end+1) = EE_err;
            distance_obs(end+1) = planner.GetCircleClearance;
        end
    end
    planner.Reset(real_robot.GetState(), heuristic, U_traj(:, k+1:end), t_traj(k+1:end));
    iteration = iteration + 1;
    
    if (EE_err <= error_tol)
        goal_reached = goal_reached +1;
        if (goal_reached > size(goals, 2)), break;end
        goal = goals(:, goal_reached);
        planner.SetGoal(goal);
        EE_err = Inf;
        display("change goal");
    end 
end
fprintf("goal reached!\n");
