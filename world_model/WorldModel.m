classdef WorldModel < matlab.mixin.Copyable
    %This class modelize the world with all the obstacles
    % it is used for the obstacle avoidace
    
    properties
        %obstacles
        obstacles;
    end
    
    methods
        function obj = WorldModel()
            %UNTITLED Construct an instance of this class
            obj.obstacles = {};
        end
        
        function UpdateState(obj, varargin)
            %update the state of the world given the data from sensor
            % it takes has input the list of obstacle and their properties
            obj.obstacles = {};
            inputs = varargin{1};
            for i = 1:5:size(inputs,2)
                obj.obstacles{end+1} = [inputs(i), inputs(i+1), inputs(i+2), inputs(i+3), inputs(i+4)]; 
            end   
        end
        
        function obstacle_list = GetState(obj)
            %return the state of the obstacles
            obstacle_list = cell(1,size(obj.obstacles, 2));
            for i= 1:size(obj.obstacles, 2)
                obstacle = obj.obstacles{i};
                obstacle_list{i} = obstacle(1:3);
            end
            % for future: not return velocities
        end
        
        function obstacle_list = GetLocalState(obj, robot_pose)
            obstacle_list = {};
            for i= 1:size(obj.obstacles, 2)
                obstacle = obj.obstacles{i};
                if norm(obstacle(1:2) - robot_pose') <= 3 + obstacle(3)
                    obstacle_list{end+1} = obstacle(1:3);
                end
            end
        end
        
        function Step(obj, delta_t)
            %it update the predicted state of the world
            for i=1:size(obj.obstacles, 2)
                obs_state = obj.obstacles{i};
                obs_state = obs_state + [obs_state(4:5), 0, 0, 0] * delta_t;
                obj.obstacles{i} = obs_state;
            end
        end
    end
end

