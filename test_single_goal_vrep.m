addpath common;
addpath dynamic_model;
addpath world_model;
addpath motion_planner;
addpath render;
addpath vrep_lib;


dynamic_parameters;
kinematic_parameters;

%step time of vrep simulation expressed in second
step_time_vrep = 0.05;

simulation_iteration = 200;

%parameters for the planner
%goal: point desidered to reach [xe, ye, xb, yb]
%delta_t is the static time step for the planner
%h is the time horizon of prediction
%N is the number of the tree expansion done by the algorithm
%pruning is a flag to enable the pruning in the algorithm
%heuristic is a flag to enable a heuristic for finding good trajectories
%we, wb, wt, are weights for the cost function in order
%we: weight for error in end_effector positioning
%wb: weight for error in base positioning
%wt: weight for counting the effect of big inputs
%error_tol: tollerance to stop at the goal
%k: how many of the predicted input to apply to real robot
goal          = [3; 3; 0; 0];
delta_t         = 0.3;
h         = 2;
N         = 200;
heuristic = true;



we          = 400;
wu          = 1;
weev        = 250;
wbv         = 4000;%4000;
w_cclear    = 2000;
cclear_min  = 0.5;
distance_to_switch = 0.1;

k         = 1;

iteration = 1;

%model of the real robot, to enable the vrep one change these lines
vrep_robot = VrepConnector(19999, step_time_vrep);

vrep_robot.ApplyControl([0;0;0;0],0.5);

%state of the world == obstacles
world = WorldModel();
world_state = [];
world.UpdateState(world_state);

%planner = Planner(vrep_robot.GetState(), world, delta_t, h, N, we, wu, weev, wbv, w_cclear, cclear_min, distance_to_switch);
planner = PlannerHard(vrep_robot.GetState(), world, delta_t, h, N, we, wu, weev, wbv, distance_to_switch);
planner.SetGoal(goal);

% %stats for plots
% tic_toc = [];
% input_traj = {};
% v_traj = {};
% EEv_traj = {};
% time_traj = {};
% distance = [];
% trajectory = {};
% base_trajectory= {};
% time = 0;

for i=1:simulation_iteration
    fprintf('Iteration: %i--------------------------------\n', iteration);
%     elapse_time = tic();
    [U_traj, t_traj] = planner.ControlTrajectory();
%     time_to_decision = toc(elapse_time);
%     tic_toc(end+1) = time_to_decision;
    if planner.ID_best ~= 1
        %path found apply first k control step
        for i = 1:k
            if (i > size(U_traj, 2)); break, end

            vrep_robot.ApplyControl(U_traj(:, i), t_traj(i));

%             direct_kin = real_robot.DirectKinematic(real_robot.q);
%             diff_direct_kin = real_robot.DifferentialDirectKinematic(real_robot.q, real_robot.v);
%             time = time + t_traj(i);
%              
%             EE_err = norm(direct_kin(1:2) - goal(1:2));
%             velocity_err = norm(diff_direct_kin(1:2)- goal(3:4));
%             fprintf('end effector error: %.3f velocity error: %.3f\n', EE_err, velocity_err);
%             input_traj{end+1} = U_traj(:,i);
%             v_traj{end+1} = real_robot.v;
%             EEv_traj{end+1} = diff_direct_kin(1:2);
%             time_traj{end+1} = time;
%             trajectory{end+1} = direct_kin(1:2);
%             base_trajectory{end+1} = direct_kin(3:4) - lg * [cos(real_robot.q(3)); sin(real_robot.q(3))];
%             distance(end+1) = EE_err;
        end
    end
    vrep_robot.GetState()
    planner.Reset(vrep_robot.GetState(), heuristic, U_traj(:, k+1:end), t_traj(k+1:end));
 
    iteration = iteration + 1;
end
vrep_robot.Close()
fprintf("goal reached!\n");
