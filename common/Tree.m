classdef Tree < handle
    % Data structure for represent trees
    properties
        dict;
    end
    
    methods
      function obj = Tree(~)
          %Initialize an empty tree
          obj.dict = {};
      end
      
      function node_ID = AddNode(obj, parent_id, data)
          %Add a node in the tree as child of the node parent_id, and
          %containing the data data. It returns the node_ID of the new node
          obj.dict{end+1} = {parent_id, data, []};
          if size(obj.dict, 2) > 1
              obj.dict{parent_id}{3} = [obj.dict{parent_id}{3} size(obj.dict, 2)];
          end
          node_ID = GetLength(obj);
      end
      
      function flag = IsLeaf(obj, node_ID)
          %Check whether a node node_ID is a leaf of the tree
          %it returns true if the node is a leaf, false otherwise 
          flag = false;
          if numel(obj.GetChildren(node_ID)) == 0
              flag = true;
          end
      end
      
      function DeleteNode(obj, node_ID)
          %Delete a node from the tree with node_ID, the node has to be in the tree 
          parent_ID = obj.GetParent(node_ID);
          children_ID = obj.GetChildren(parent_ID);
          obj.dict{parent_ID}{3} = children_ID(children_ID ~= node_ID);
          obj.dict(node_ID) = [];
      end
      
      function len = GetLength(obj)
          %Return the number of nodes in the tree
          len = size(obj.dict, 2);
      end
      
      function parent_ID = GetParent(obj, node_ID)
          %Given a node node_ID, it returns the parent parent_ID
          parent_ID = obj.dict{node_ID}{1};
      end
      
      function data = GetData(obj, node_ID)
          %Given the node node_ID, it returns the data contained in the
          %node
          data = obj.dict{node_ID}{2};
      end
      
      function children_IDs = GetChildren(obj, node_ID)
          %Given a node node_ID, it returns a list of 
          children_IDs = obj.dict{node_ID}{3};
      end
      
      function path = ExtractPath(obj, leaf_ID, root_ID)
          path = [leaf_ID];
          while leaf_ID ~= root_ID
              path = [obj.dict{leaf_ID}{1}, path];
              leaf_ID = obj.dict{leaf_ID}{1};
          end
      end
      
      function render = Plot(obj)
          g = graph;
          g = g.addnode(obj.GetLength());
         
          for i = 1:obj.GetLength()
              c = obj.GetChildren(i);
              for j = 1:size(c, 2)
                  g = g.addedge(i, c(j));
              end
          end
          render = plot(g);
      end
      
      function current_pruned = Prune(obj,node_ID)
          current_pruned = [node_ID];
          index = 1;
          current_node = current_pruned(index);

          while index <= numel(current_pruned)
              current_pruned = [current_pruned obj.GetChildren(current_node)];
              index = index + 1;
              if index > numel(current_pruned) break; end
              current_node = current_pruned(index); 
          end
      end
      
      function near_node = SelectNearestNode(obj, state, unpruned)
          min = Inf;
          for i=1:size(unpruned)
              data = obj.GetData(unpruned(i));
              distance = obj.DistanceFunction(data{1}, state);
              if(distance < min)
                  min = distance;
                  near_node = i;
              end
          end
      end
      
      function distance = DistanceFunction(obj, node_1, node_2)
          distance = zeros(18,1);
          distance(1:2) = node_1(1:2) - node_2(1:2);
          distance(3:6) = reshape(AngleToRot(node_1(3)),4,1) - reshape(AngleToRot(node_2(3)),4,1);
          distance(7:10) = reshape(AngleToRot(node_1(4)),4,1) - reshape(AngleToRot(node_2(4)),4,1);
          distance(11:14) = reshape(AngleToRot(node_1(5)),4,1) - reshape(AngleToRot(node_2(5)),4,1);
          distance(15:18) = node_1(6:9) - node_2(6:9);
          distance = distance' * distance;
      end
      
    end
end

