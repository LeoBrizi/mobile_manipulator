% DYNAMIC_COMPUTATION: 
%   this file contains the computations needed to write the DynamicModel class

clear variables;
clc;

syms t x_g(t) x_a(t) x_b(t) y_g(t) y_a(t) y_b(t) 
syms x_f(t) y_f(t) phi(t) theta_1(t) theta_2(t) x_f_d(t) y_f_d(t) phi_d(t) theta_1_d(t) theta_2_d(t)
syms lg l1 l11 l2 l21 m0 m1 m2 j0 j1 j2 r b

%*************************** Euler-Lagrange eq in terms of q, q° and q°° ***************************

x_b = x_f + l1 * cos(phi + theta_1) + l21 * cos(phi + theta_1 + theta_2);
y_b = y_f + l1 * sin(phi + theta_1) + l21 * sin(phi + theta_1 + theta_2);

x_a = x_f + l11 * cos(phi + theta_1);
y_a = y_f + l11 * sin(phi + theta_1);

x_g = x_f - lg * cos(phi);
y_g = y_f - lg * sin(phi);

x_g_d = x_f_d + lg * sin(phi) * phi_d;
y_g_d = y_f_d - lg * cos(phi) * phi_d;

x_a_d = x_f_d - l11 * sin(phi + theta_1) * (phi_d + theta_1_d);
y_a_d = y_f_d + l11 * cos(phi + theta_1) * (phi_d + theta_1_d);

x_b_d = x_f_d - l1 * sin(phi + theta_1) * (phi_d + theta_1_d) - l21*sin(phi + theta_1 + theta_2) * (phi_d + theta_1_d + theta_2_d);
y_b_d = y_f_d + l1 * cos(phi + theta_1) * (phi_d + theta_1_d) + l21*cos(phi + theta_1 + theta_2) * (phi_d + theta_1_d + theta_2_d);

lagrangian = 1/2 * m0 * (x_g_d^2 + y_g_d^2) + 1/2 * j0 * phi_d^2 + 1/2 * m1 * (x_a_d^2 + y_a_d^2) + ...
            1/2 * j1 * (phi_d + theta_1_d)^2 + 1/2 * m2 * (x_b_d^2 + y_b_d^2) + ...
            1/2 * j2 * (phi_d + theta_1_d + theta_2_d)^2;

q = [x_f, y_f, phi, theta_1, theta_2];
q_d = [x_f_d, y_f_d, phi_d, theta_1_d, theta_2_d];

D_L_d_q = functionalDerivative(lagrangian, q);
D_L_d_qd = functionalDerivative(lagrangian, q_d);
D_L_d_t = diff(D_L_d_qd, t);

EL_eq=simplify(D_L_d_t - D_L_d_q);
display(EL_eq);

%********************************************* Matrix J ********************************************

R = [ cos(phi),  -sin(phi),         0,          0;
      sin(phi),   cos(phi),         0,          0;
             0,          0,  cos(phi),  -sin(phi);
             0,          0,  sin(phi),   cos(phi); ];
       
J11 = -l1*sin(theta_1) -l2*sin(theta_1+theta_2);
J12 = -l2*sin(theta_1+theta_2);
J21 = l1*cos(theta_1)+l2*cos(theta_1+theta_2);
J22 = l2*cos(theta_1+theta_2);

Ja = [ r/2 - J11*r/b,  r/2 + J11*r/b,  J11,    J12;
       -(lg+J21)*r/b,   (lg+J21)*r/b,  J21,    J22;
                 r/2,            r/2,    0,      0;
             -lg*r/b,         lg*r/b,    0,      0;];
       
J = simplify(R*Ja);
display(J);

% J = [ cos(phi)*(r/2+(r*(l1*sin(theta_1)+l2*sin(theta_1+theta_2)))/b)+(r*sin(phi)*(lg+l1*cos(theta_1)+l2*cos(theta_1 + theta_2)))/b,     cos(phi)*(r/2-(r*(l1*sin(theta_1)+l2*sin(theta_1+theta_2)))/b)-(r*sin(phi)*(lg+l1*cos(theta_1)+l2*cos(theta_1+theta_2)))/b,    -l2*sin(phi+theta_1+theta_2)-l1*sin(phi+theta_1),      -l2*sin(phi+theta_1+theta_2);
%       sin(phi)*(r/2+(r*(l1*sin(theta_1)+l2*sin(theta_1+theta_2)))/b)-(r*cos(phi)*(lg+l1*cos(theta_1)+l2*cos(theta_1 + theta_2)))/b,   sin(phi)*(r/2-(r*(l1*sin(theta_1)+l2*sin(theta_1 + theta_2)))/b)+(r*cos(phi)*(lg+l1*cos(theta_1)+l2*cos(theta_1+theta_2)))/b,   l2*cos(phi+theta_1+theta_2)+l1*cos(phi + theta_1),   l2*cos(phi + theta_1 + theta_2);
%                                                                                                   (r*cos(phi))/2+(lg*r*sin(phi))/b,                                                                                               (r*cos(phi))/2-(lg*r*sin(phi))/b,                                                   0,                                 0;
%                                                                                                   (r*sin(phi))/2-(lg*r*cos(phi))/b,                                                                                               (r*sin(phi))/2+(lg*r*cos(phi))/b,                                                   0,                                 0;];

%***************************************** Matrix S and S° *****************************************

S = [ r*cos(phi)/2+lg*r*sin(phi)/b,  r*cos(phi)/2-lg*r*sin(phi)/b,  0,  0;
      r*sin(phi)/2-lg*r*cos(phi)/b,  r*sin(phi)/2+lg*r*cos(phi)/b,  0,  0;
                              -r/b,                           r/b,  0,  0;
                                 0,                             0,  1,  0;
                                 0,                             0,  0,  1;];
S_d = simplify(diff(S, t));
display(S_d);

                                                                                                        
