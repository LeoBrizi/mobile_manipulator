syms x_f y_f phi theta_1 theta_2 theta_l theta_r phi_d theta_l_d theta_r_d theta_1_d theta_2_d real 
syms lg m0 l11 l1 l21 l2 m1 m2 j0 j1 j2 r b real positive
syms tau_1 tau_2 tau_3 tau_4 real


S = [ (r*cos(phi))/2 + (lg*r*sin(phi))/b, (r*cos(phi))/2 - (lg*r*sin(phi))/b, 0, 0;
    (r*sin(phi))/2 - (lg*r*cos(phi))/b, (r*sin(phi))/2 + (lg*r*cos(phi))/b, 0, 0;
                                      -r/b,                                      r/b, 0, 0;
                                         0,                                        0, 1, 0;
                                         0,                                        0, 0, 1;];
                                     
                                     
B = [                                                                                                                        m0 + m1 + m2,                                                                                                                                   0, lg*m0*sin(phi) - l21*m2*sin(phi + theta_1 + theta_2) - l1*m2*sin(phi + theta_1) - l11*m1*sin(phi + theta_1), - l21*m2*sin(phi + theta_1 + theta_2) - l1*m2*sin(phi + theta_1) - l11*m1*sin(phi + theta_1), -l21*m2*sin(phi + theta_1 + theta_2);
                                                                                                                                   0,                                                                                                                        m0 + m1 + m2, l21*m2*cos(phi + theta_1 + theta_2) - lg*m0*cos(phi) + l1*m2*cos(phi + theta_1) + l11*m1*cos(phi + theta_1),   l21*m2*cos(phi + theta_1 + theta_2) + l1*m2*cos(phi + theta_1) + l11*m1*cos(phi + theta_1),  l21*m2*cos(phi + theta_1 + theta_2);
 lg*m0*sin(phi) - l21*m2*sin(phi + theta_1 + theta_2) - l1*m2*sin(phi + theta_1) - l11*m1*sin(phi + theta_1), l21*m2*cos(phi + theta_1 + theta_2) - lg*m0*cos(phi) + l1*m2*cos(phi + theta_1) + l11*m1*cos(phi + theta_1),                                                m2*l1^2 + 2*m2*cos(theta_2)*l1*l21 + m1*l11^2 + m2*l21^2 + m0*lg^2 + j0 + j1 + j2,                                             m2*l1^2 + 2*m2*cos(theta_2)*l1*l21 + m1*l11^2 + m2*l21^2 + j1 + j2,     m2*l21^2 + l1*m2*cos(theta_2)*l21 + j2;
                   - l21*m2*sin(phi + theta_1 + theta_2) - l1*m2*sin(phi + theta_1) - l11*m1*sin(phi + theta_1),                     l21*m2*cos(phi + theta_1 + theta_2) + l1*m2*cos(phi + theta_1) + l11*m1*cos(phi + theta_1),                                                               m2*l1^2 + 2*m2*cos(theta_2)*l1*l21 + m1*l11^2 + m2*l21^2 + j1 + j2,                                             m2*l1^2 + 2*m2*cos(theta_2)*l1*l21 + m1*l11^2 + m2*l21^2 + j1 + j2,     m2*l21^2 + l1*m2*cos(theta_2)*l21 + j2;
                                                                                       -l21*m2*sin(phi + theta_1 + theta_2),                                                                                        l21*m2*cos(phi + theta_1 + theta_2),                                                                                           m2*l21^2 + l1*m2*cos(theta_2)*l21 + j2,                                                                         m2*l21^2 + l1*m2*cos(theta_2)*l21 + j2,                                 m2*l21^2 + j2;];
                                                                                   
                                                                                  
                                                                                   
                                                                                   
                                                                                   
 S_d = [ (r*(2*lg*cos(phi) - b*sin(phi))*phi_d)/(2*b), -(r*(2*lg*cos(phi) + b*sin(phi))*phi_d)/(2*b), 0, 0;
 (r*(b*cos(phi) + 2*lg*sin(phi))*phi_d)/(2*b),  (r*(b*cos(phi) - 2*lg*sin(phi))*phi_d)/(2*b), 0, 0;
                                                            0,                                                             0, 0, 0;
                                                            0,                                                             0, 0, 0;
                                                            0,                                                             0, 0, 0;];
 
 
 V =[lg*m0*cos(phi)*phi_d^2 - cos(phi + theta_1)*(phi_d + theta_1_d)*(l1*m2*phi_d + l11*m1*phi_d + l1*m2*theta_1_d + l11*m1*theta_1_d) - cos(phi + theta_1 + theta_2)*(phi_d + theta_1_d + theta_2_d)*(l21*m2*phi_d + l21*m2*theta_1_d + l21*m2*theta_2_d);
 lg*m0*sin(phi)*phi_d^2 - sin(phi + theta_1 + theta_2)*(phi_d + theta_1_d + theta_2_d)*(l21*m2*phi_d + l21*m2*theta_1_d + l21*m2*theta_2_d) - sin(phi + theta_1)*(phi_d + theta_1_d)*(l1*m2*phi_d + l11*m1*phi_d + l1*m2*theta_1_d + l11*m1*theta_1_d);
                                                                                                                                                                                                                           -l1*l21*m2*sin(theta_2)*theta_2_d*(2*phi_d + 2*theta_1_d + theta_2_d);
                                                                                                                                                                                                                           -l1*l21*m2*sin(theta_2)*theta_2_d*(2*phi_d + 2*theta_1_d + theta_2_d);
                                                                                                                                                                                                                                                          l1*l21*m2*sin(theta_2)*(phi_d + theta_1_d)^2];
                                                       
                                                        
 v = [theta_l_d; theta_r_d; theta_1_d; theta_2_d]; 
 tau = [tau_1; tau_2; tau_3; tau_4];
 q = [x_f; y_f; phi; theta_1; theta_2];
 M = simplify(S' * B * S); 
 V_star = simplify(S' * (B * S_d * v + V));
 
 
 dM1_dq = - inv(M) * (diff(M, x_f) + diff(M, y_f) + diff(M, phi) + diff(M, theta_1) + diff(M, theta_2)) * inv(M);
 
 dM1_dq = simplify(dM1_dq);
 
 dq_dot_dv = S;
 
 dq_dot_dq = diff(S,phi) * v;
 
 dv_dot_dv = simplify(- inv(M)*jacobian(V_star, v));
 
 dv_dot_dq = simplify(dM1_dq * ( eye(4,4) * tau - V_star) - inv(M) * jacobian(V_star, q));
 
 
