classdef DynamicModel < matlab.mixin.Copyable
    % Dynamic model of our robot
    %   as described in the paper "Planning and Model-Based Control for Mobile Manipulators"
    
    properties
        q;          %[x_f; y_f; phi; theta_1; theta_2]
        q_d;        %[x_f_d; y_f_d; phi_d; theta_1_d; theta_2_d]
        v;          %[theta_l_d; theta_r_d; theta_1_d; theta_2_d]
    end
    
    properties (Access = private)
        m0;         %base mass
        m1;         %arm link1 mass
        m2;         %arm link2 mass
        I0;         %base inertia
        I1;         %arm link1 inertia
        I2;         %arm link2 inertia
        
        l1;         %arm link1 lenght
        l11;        %distance arm link1-com
        l2;         %arm link2 lenght
        l21;        %distance arm link2-com
        lg;         %distance base-com
        b;          %base line
        r;          %wheel radius
         
        t;          %time
    end
    
    methods
        function obj = DynamicModel()
            % Construct an instance of this class
            %   We initialize the dynamic and kinematic parameters, then q, q_d, v and t variables

            obj.m0 = evalin('base',"m0");
            obj.m1 = evalin('base',"m1"); 
            obj.m2 = evalin('base',"m2");
            obj.I0 = evalin('base',"I0");
            obj.I1 = evalin('base',"I1");
            obj.I2 = evalin('base',"I2");
            
            obj.l1 = evalin('base',"l1");
            obj.l11 = evalin('base',"l11"); 
            obj.l2 = evalin('base',"l2");
            obj.l21 = evalin('base',"l21");
            obj.lg = evalin('base',"lg");
            obj.b = evalin('base',"b");
            obj.r = evalin('base',"r");
            
            obj.q = [0;
                     0;
                     0;		%rad--> use deg2rad()
                     0;		%rad--> use deg2rad()
                     0];	%rad--> use deg2rad()

            obj.q_d = [0;
                       0;
                       0;
                       0;
                       0];

            obj.v = [0;
                     0;
                     0;
                     0];

            obj.t = 0;
        end
        
        function SetState(obj, x, t)
            % set the state of the model
            obj.q = x(1:5);
            obj.v = x(6:9);
            obj.t = t;
            
        end
        
        function x = GetState(obj)
            %return the state of the model
            x = [obj.q; obj.v];
        end
        
        function Step(obj, tau, step_time)
            % Realize one step of direct dynamics
            %   INPUT: Given tau = [tau_l; tau_r; tau_1; tau_2] and time step of integration
            %   SIDE EFFECTS: Update v, q_d and q variables according to generated motion due to tau
                 
            % Integrate v_d = inv(M_star) * (-S' * (M * S_d * v_d + V) + tau) to find v values
            function dvdt = dynamic(t, v)

                % Evaluate needed matrices with current values of q and q_d
                M = obj.GetM(obj.q(3), obj.q(4), obj.q(5));
                S = obj.GetS(obj.q(3));
                S_d = obj.GetS_d(obj.q(3), obj.q_d(3));
                V = obj.GetV(obj.q(3), obj.q(4), obj.q(5), obj.q_d(3), obj.q_d(4), obj.q_d(5));
                M_star = S' * M * S;

                dvdt = [inv(M_star) * (-S' * (M * S_d * v + V) + tau)];

                % Update the values of q_d, q and t variables
				obj.q_d = S * v;
                obj.q = obj.q + obj.q_d * (t - obj.t);
                obj.q(3:5) = wrapToPi(obj.q(3:5));
                obj.t = t;
            end
          
            function [value, isterminal, direction] = saturate(t, v)
                tolerance = 5e-2;
                value      = ...%(3/4*pi - abs(obj.q(4)) <= tolerance || 3/4*pi - abs(obj.q(5)) <= tolerance || ...
                    (4.2167 - abs(v(1)) <= tolerance || 4.2167 - abs(v(2)) <= tolerance ||...
                    1.05 - abs(v(3)) <= tolerance  || 1.05 - abs(v(4)) <= tolerance );
                isterminal = 1;   % Stop the integration
                direction  = 0;
            end
            
            initial_t = obj.t;
			%reduce tollerance
			%opts = odeset('RelTol', 1e-2, 'AbsTol', 1e-3);%, 'Stats', 'on');
            %opts    = odeset('Events', @saturate);
            [t, v] = ode45(@dynamic, [obj.t obj.t + step_time], obj.v);%, opts);
            t = t(end);
%             while t < initial_t + step_time 
%                 
%                 % Update the values of v, q_d and tau variables
%                 temp_v= v(end,:)';
%                 
%                 % position constraints
%                 if (3/4*pi - obj.q(4) <= 5e-2) %round(obj.q(4),2) == round(3/4*pi,2) 
%                    obj.q(4) = wrapToPi(3/4*pi -0.06); 
%                 end
%                 if (3/4*pi + obj.q(4) <= 5e-2) %round(obj.q(4),2) == round(-3/4*pi,2) 
%                    obj.q(4) = wrapToPi(-3/4*pi +0.06); 
%                 end
%                 if (3/4*pi - obj.q(5) <= 5e-2) %round(obj.q(5),2) == round(3/4*pi,2) 
%                     obj.q(5) = wrapToPi(3/4*pi -0.06); 
%                 end
%                 if (3/4*pi + obj.q(5) <= 5e-2) %round(obj.q(5),2) == round(-3/4*pi,2) 
%                     obj.q(5) = wrapToPi(-3/4*pi +0.06); 
%                 end
%                 
%                 %velocities constraints
%                 if (4.2167 - temp_v(1) <= 5e-2) % if (temp_v(5)>4.2167)
%                     temp_v(1) = 4.1567;
%                 end
%                 if (4.2167 + temp_v(1) <= 5e-2) %if (temp_v(5)<-4.2167)
%                     temp_v(1) = -4.1567;
%                 end 
%                 if (4.2167 - temp_v(2) <= 5e-2) %if (temp_v(6)>4.2167) 
%                     temp_v(2) = 4.1567;
%                 end
%                 if (4.2167 + temp_v(2) <= 5e-2) %if (temp_v(6)<4.2167)
%                     temp_v(2) = -4.1567;
%                 end
%                 if (1.05 - temp_v(3) <= 5e-2) %if (temp_v(7)>1.05) 
%                     temp_v(3) = 0.99;
%                     obj.q_d(4) = 0.99;
%                 end
%                 if (1.05 + temp_v(3) <= 5e-2)  %if (temp_v(7)<1.05)
%                     temp_v(3) = -0.99;
%                     obj.q_d(4) = -0.99;
%                 end
%                 if (1.05 - temp_v(4) <= 5e-2)  %if (temp_v(8)>1.05) 
%                     temp_v(4) = 0.99;
%                     obj.q_d(5) = 0.99;
%                 end
%                 if (1.05 + temp_v(4) <= 5e-2) %if (temp_v(8)<1.05)
%                     temp_v(4) = -0.99;
%                     obj.q_d(5) = -0.99;
%                 end
%                 obj.v = temp_v;
%                 opts    = odeset('Events', @saturate);
%                 [t, v] = ode45(@dynamic, [t, initial_t + step_time], obj.v, opts);
%                 t = t(end);
%             end
            
            % Update the values of v variable
            obj.v = v(end,:)';
        end
        
        function Integrate(obj, tau, step_time)
            %tutto ciò che è di obj è condizione iniziale
            S = obj.GetS(obj.q(3));
            qk_dot = S * obj.v;
            
            M = obj.GetM(obj.q(3), obj.q(4), obj.q(5));
            S_d = obj.GetS_d(obj.q(3), qk_dot(3));
            V = obj.GetV(obj.q(3), obj.q(4), obj.q(5), qk_dot(3), qk_dot(4), qk_dot(5));
            M_star = S' * M * S;
            vk_dot = M_star \ (tau - S' * (M * S_d * obj.v + V));

            obj.q = obj.q + step_time * qk_dot;
            %obj.q(3:5) = wrapToPi(obj.q(3:5));
            obj.v = obj.v + step_time * vk_dot;
            obj.t = obj.t + step_time;
            obj.q_d = qk_dot;
        end
        
        function x = DirectKinematic(obj, q)
			% Compute direct kinematic q->x
			%	INPUT: Robot, i.e. current state q and kinematic parameters l1 and l2
			%	OUTPUT: x = x_e, y_e, x_f, y_f
            x_f = q(1);
            y_f = q(2);
            x_e = x_f + obj.l1 * cos(q(4) + q(3)) + obj.l2 * cos(q(4) + q(5) + q(3));
            y_e = y_f + obj.l1 * sin(q(4) + q(3)) + obj.l2 * sin(q(4) + q(5) + q(3));
            x = [x_e; 
                 y_e;
                 x_f;
                 y_f;];
        end
        
        function x_d = DifferentialDirectKinematic(obj, q, v)
			% Compute differential direct kinematic q_d->x_d
			%	INPUT: Robot, i.e. current state q_d
			%	OUTPUT: x_d = x_e_d, y_e_d, x_f_d, y_f_d
            %q_d = obj.GetS(q(3)) * v;
            x_d = obj.GetJ(q(3), q(4), q(5)) * v;
        end
        
        function q = InverseKinematicEE(obj, goal, step_size, tol)
            q_k = obj.q;
            delta = 100;
            while norm(delta) > tol
                J = obj.GetJee(q_k(3), q_k(4), q_k(5));
                direct_kin = obj.DirectKinematic(q_k);
                delta = step_size * J' *(goal - direct_kin(1:2));
                q_k = q_k + delta;
            end
            q = q_k;
        end
        
        function [driving_v, steering_v] = GetBaseVelocity(obj, v)
            driving_v = obj.r * (v(2) + v(1)) / 2;
            steering_v = obj.r * (v(2) - v(1)) / obj.b;
        end
        
        function radii = GetRadii(obj)
            r1 = sqrt(obj.lg^2 + (obj.b/2)^2);
            r2 = obj.l11;
            r3 = obj.l2/2;
            radii = [r1, r2, r3];
        end
        
        function centers = GetCenters(obj)
          
            x_1 = obj.q(1) - obj.lg * cos(obj.q(3));
            y_1 = obj.q(2) - obj.lg * sin(obj.q(3));
            c1 = [x_1; y_1];

            x_2 = obj.q(1) + obj.l11 * cos(obj.q(4) + obj.q(3));
            y_2 = obj.q(2) + obj.l11 * sin(obj.q(4) + obj.q(3));
            c2 = [x_2; y_2];

            x_3 = obj.q(1) + obj.l1 * cos(obj.q(4) + obj.q(3)) + obj.l2/2 * cos(obj.q(4) + obj.q(5) + obj.q(3));
            y_3 = obj.q(2) + obj.l1 * sin(obj.q(4) + obj.q(3)) + obj.l2/2 * sin(obj.q(4) + obj.q(5) + obj.q(3));
            c3 = [x_3; y_3];
            
            centers= [c1, c2, c3];
        end
        
        function [A, B] = LinearizeDynamic(obj, x, u)
            
            A = zeros(9, 9);
            B = zeros(9, 4);
            eps = 1e-8;
            xperturb_plus = x;
            xperturb_minus = x;
            for i = 1:9
                xperturb_plus(i) = xperturb_plus(i) + eps;
                xperturb_minus(i) = xperturb_minus(i) - eps;
                
                M_plus = obj.GetM(xperturb_plus(3), xperturb_plus(4), xperturb_plus(5));
                M_minus = obj.GetM(xperturb_minus(3), xperturb_minus(4), xperturb_minus(5));
                S_plus = obj.GetS(xperturb_plus(3));
                S_minus = obj.GetS(xperturb_minus(3));
                q_d_plus = S_plus * xperturb_plus(6:9);
                q_d_minus = S_minus * xperturb_minus(6:9);
                V_plus = obj.GetV(xperturb_plus(3), xperturb_plus(4), xperturb_plus(5), q_d_plus(3), q_d_plus(4), q_d_plus(5));
                V_minus = obj.GetV(xperturb_minus(3), xperturb_minus(4), xperturb_minus(5), q_d_minus(3), q_d_minus(4), q_d_minus(5));
                S_d_plus = obj.GetS_d(xperturb_plus(3), q_d_plus(3));
                S_d_minus = obj.GetS_d(xperturb_minus(3), q_d_minus(3));
                M_star_plus_inv = inv(S_plus' * M_plus * S_plus);
                M_star_minus_inv = inv(S_minus' * M_minus * S_minus);
                
                feval_plus =  [ q_d_plus;  M_star_plus_inv * u - M_star_plus_inv * S_plus' * (M_plus * S_d_plus * xperturb_plus(6:9) + V_plus)];
                feval_minus = [q_d_minus;  M_star_minus_inv * u - M_star_minus_inv * S_minus' * (M_minus * S_d_minus * xperturb_minus(6:9) + V_minus)];
                
                A(:, i) = (feval_plus - feval_minus) / (2 * eps);
                
                xperturb_plus(i) = x(i);
                xperturb_minus(i) = x(i);
            end
            
            S = obj.GetS(x(3));
            M = obj.GetM(x(3), x(4), x(5));
            M_star_inv = inv(S' * M * S);
            
            u_perturb_plus = u;
            u_perturb_minus = u;
            for i = 1:4
                u_perturb_plus(i) = u_perturb_plus(i) + eps;
                u_perturb_minus(i) = u_perturb_minus(i) -eps;
                
                feval_plus = [ 0; 0; 0; 0; 0; M_star_inv * u_perturb_plus];
                feval_minus = [ 0; 0; 0; 0; 0; M_star_inv * u_perturb_minus];
                
                B(:, i)= (feval_plus - feval_minus) / (2 * eps);
                
                u_perturb_plus(i) = u(i);
                u_perturb_minus(i) = u(i);
            end
        end
    end
    
    methods(Access = private)
        function M = GetM(obj, phi, theta_1, theta_2)
            % Evaluate M matrix with current values (=input)
            s012 = sin(phi+theta_1+theta_2);
            c012 = cos(phi+theta_1+theta_2);
            s01 = sin(phi+theta_1);
            c01 = cos(phi+theta_1);
            m11 = obj.m0 + obj.m1 + obj.m2;
            m12 = 0;
            m13 = obj.lg*obj.m0*sin(phi)-obj.l21*obj.m2*s012-obj.l1*obj.m2*s01-obj.l11*obj.m1*s01;
            m14 = -obj.l21*obj.m2*s012-obj.l1*obj.m2*s01-obj.l11*obj.m1*s01;
            m15 = -obj.l21*obj.m2*s012;
            
            m22 = m11;
            m23 = -obj.lg*obj.m0*cos(phi)+obj.l21*obj.m2*c012+obj.l1*obj.m2*c01+obj.l11*obj.m1*c01;
            m24 = obj.l21*obj.m2*c012+obj.l1*obj.m2*c01+obj.l11*obj.m1*c01;
            m25 = obj.l21*obj.m2*c012;
            
            m33 = obj.I0+obj.I1+obj.I2+obj.l1^2*obj.m2+obj.l11^2*obj.m1+obj.l21^2*obj.m2+obj.lg^2*obj.m0+2*obj.l1*obj.l21*obj.m2*cos(theta_2);
            m34 = obj.I1+obj.I2+obj.l1^2*obj.m2+obj.l11^2*obj.m1+obj.l21^2*obj.m2+2*obj.l1*obj.l21*obj.m2*cos(theta_2);
            m35 = obj.I2+obj.l21^2*obj.m2+obj.l1*obj.l21*obj.m2*cos(theta_2);
            
            m44 = m34;
            m45 = m35;
            
            m55 = obj.I2+obj.l21^2*obj.m2;
            
            M = [m11, m12, m13, m14, m15;
                 m12, m22, m23, m24, m25;
                 m13, m23, m33, m34, m35;
                 m14, m24, m34, m44, m45;
                 m15, m25, m35, m45, m55;];
        end
        
        function V = GetV(obj, phi, theta_1, theta_2, phi_d, theta_1_d, theta_2_d)
            % Evaluate V matrix with current values (=input)
            temp0 = cos(phi + theta_1 + theta_2)*(phi_d + theta_1_d + theta_2_d);
            temp1 = cos(phi + theta_1)*(phi_d + theta_1_d);
            temp2 = sin(phi + theta_1 + theta_2)*(phi_d + theta_1_d + theta_2_d);
            temp4 = sin(phi + theta_1)*(phi_d + theta_1_d);
            V = [ obj.lg*obj.m0*phi_d^2*cos(phi) + (- obj.l21*obj.m2*phi_d - obj.l21*obj.m2*theta_1_d - obj.l21*obj.m2*theta_2_d)*temp0 + (- obj.l1*obj.m2*phi_d - obj.l11*obj.m1*phi_d - obj.l1*obj.m2*theta_1_d - obj.l11*obj.m1*theta_1_d)*temp1;
                  obj.lg*obj.m0*phi_d^2*sin(phi) + (- obj.l21*obj.m2*phi_d - obj.l21*obj.m2*theta_1_d - obj.l21*obj.m2*theta_2_d)*temp2 + (- obj.l1*obj.m2*phi_d - obj.l11*obj.m1*phi_d - obj.l1*obj.m2*theta_1_d - obj.l11*obj.m1*theta_1_d)*temp4;
                                                                                                                                                                  -obj.l1*obj.l21*obj.m2*theta_2_d*sin(theta_2)*(2*phi_d + 2*theta_1_d + theta_2_d);
                                                                                                                                                                  -obj.l1*obj.l21*obj.m2*theta_2_d*sin(theta_2)*(2*phi_d + 2*theta_1_d + theta_2_d);
                                                                                                                                                                                           obj.l1*obj.l21*obj.m2*sin(theta_2)*(phi_d + theta_1_d)^2;];
        end
        
        function S = GetS(obj, phi)
            % Evaluate S matrix with current values (=input)
            
            S = [ obj.r*cos(phi)/2+obj.lg*obj.r*sin(phi)/obj.b,   obj.r*cos(phi)/2-obj.lg*obj.r*sin(phi)/obj.b,   0,   0;
                  obj.r*sin(phi)/2-obj.lg*obj.r*cos(phi)/obj.b,   obj.r*sin(phi)/2+obj.lg*obj.r*cos(phi)/obj.b,   0,   0;
                                                  -obj.r/obj.b,                                    obj.r/obj.b,   0,   0;
                                                             0,                                              0,   1,   0;
                                                             0,                                              0,   0,   1;];
        end
        
        function S_d = GetS_d(obj, phi, phi_d)
            % Evaluate S_d matrix with current values (=input)
            
            S_d = [ (obj.lg*obj.r*cos(phi)*phi_d)/obj.b-(obj.r*sin(phi)*phi_d)/2,   - (obj.r*sin(phi)*phi_d)/2-(obj.lg*obj.r*cos(phi)*phi_d)/obj.b,   0,   0;
                    (obj.r*cos(phi)*phi_d)/2+(obj.lg*obj.r*sin(phi)*phi_d)/obj.b,     (obj.r*cos(phi)*phi_d)/2-(obj.lg*obj.r*sin(phi)*phi_d)/obj.b,   0,   0;
                                                                               0,                                                                0,   0,   0;
                                                                               0,                                                                0,   0,   0;
                                                                               0,                                                                0,   0,   0];
        end
        
        function J = GetJ(obj, phi, theta_1, theta_2)
            % Evaluate J matrix with current values (=input)
            
            J = [ cos(phi)*(obj.r/2+(obj.r*(obj.l1*sin(theta_1)+obj.l2*sin(theta_1+theta_2)))/obj.b)+(obj.r*sin(phi)*(obj.lg+obj.l1*cos(theta_1)+obj.l2*cos(theta_1 + theta_2)))/obj.b,      cos(phi)*(obj.r/2-(obj.r*(obj.l1*sin(theta_1)+obj.l2*sin(theta_1+theta_2)))/obj.b)-(obj.r*sin(phi)*(obj.lg+obj.l1*cos(theta_1)+obj.l2*cos(theta_1+theta_2)))/obj.b,    -obj.l2*sin(phi+theta_1+theta_2)-obj.l1*sin(phi+theta_1),      -obj.l2*sin(phi+theta_1+theta_2);
                  sin(phi)*(obj.r/2+(obj.r*(obj.l1*sin(theta_1)+obj.l2*sin(theta_1+theta_2)))/obj.b)-(obj.r*cos(phi)*(obj.lg+obj.l1*cos(theta_1)+obj.l2*cos(theta_1 + theta_2)))/obj.b,    sin(phi)*(obj.r/2-(obj.r*(obj.l1*sin(theta_1)+obj.l2*sin(theta_1 + theta_2)))/obj.b)+(obj.r*cos(phi)*(obj.lg+obj.l1*cos(theta_1)+obj.l2*cos(theta_1+theta_2)))/obj.b,   obj.l2*cos(phi+theta_1+theta_2)+obj.l1*cos(phi + theta_1),   obj.l2*cos(phi + theta_1 + theta_2);
                                                                                                                                      (obj.r*cos(phi))/2+(obj.lg*obj.r*sin(phi))/obj.b,                                                                                                                        (obj.r*cos(phi))/2-(obj.lg*obj.r*sin(phi))/obj.b,                                                           0,                                     0;
                                                                                                                                      (obj.r*sin(phi))/2-(obj.lg*obj.r*cos(phi))/obj.b,                                                                                                                        (obj.r*sin(phi))/2+(obj.lg*obj.r*cos(phi))/obj.b,                                                           0,                                     0;];
        end
        
        function J = GetJee(obj, phi, theta_1, theta_2)
            J11 = -obj.l1*sin(theta_1) - obj.l2*sin(theta_1 + theta_2);
            J12 = -obj.l2*sin(theta_1 + theta_2);
            J21 = obj.l1*cos(theta_1) + obj.l2*cos(theta_1 + theta_2);
            J22 = obj.l2*cos(theta_1 + theta_2);
            J = [1, 0, cos(phi)*J11 - sin(phi)*J21, cos(phi)*J11 - sin(phi)*J21, cos(phi)*J12 - sin(phi)*J22;
                 0, 1, sin(phi)*J11 + cos(phi)*J21, sin(phi)*J11 + cos(phi)*J21, sin(phi)*J12 + cos(phi)*J22];
        end
    end 
end

