clear;
clc;
num_esperiments = 1;
%rng(3896);

total_tictoc = {};
total_input_traj = {};
total_q_traj = {};
total_q_d_traj = {};
total_v_traj = {};
total_EEv_traj = {};
total_time_traj = {};
total_distance_goal = {};
total_distance_goal_it = {};
total_distance_obs = {};
total_distance_obs_it = {};
total_traj = {};
total_base_traj = {};
iteration_done = [];

for i = 1:num_esperiments
    elapse_t = tic;
    %Sample_MPC_with_LQR_reg;
    SampleMPC;
    time_to_complete = toc(elapse_t);
    
    total_tictoc{end+1} = tic_toc;
    total_input_traj{end+1} = input_traj;
    total_q_traj{end+1} = q_traj;
    total_q_d_traj{end+1} = q_d_traj;
    total_v_traj{end+1} = v_traj;
    total_EEv_traj{end+1} = EEv_traj;
    total_time_traj{end+1} = time_traj;
    total_distance_goal{end+1} = distance_goal;
    total_distance_goal_it{end+1} = distance_goal_it;
    total_distance_obs{end+1} = distance_obs;
    total_distance_obs_it{end+1} = distance_obs_it;
    total_traj{end+1} = trajectory;
    total_base_traj{end+1} = base_trajectory;
    iteration_done(end+1) = iteration;
end

%%%%%%%%%%%%%%%%%%%%% PLOTS %%%%%%%%%%%%%%%%%%%%%%%
clf;
%time
omin = inf;
omax = -inf;
for i=1:size(total_tictoc,2)
    ct = cell2mat(total_tictoc(i));
    c_max = max(ct); c_min = min(ct);
    if c_max > omax, omax = c_max; end
    if c_min < omin, omin = c_min; end
end

y = [omin;omax];
b = bar(1:2, y);
hold on; grid minor;
line([0, 3], [0.1, 0.1], ... 
    'LineWidth', 0.05, ...
    'Color','red', ...
    'LineStyle','--');
%title('Best and worst MPC iteration time');
set(gca,'xticklabel', {'Best','Worst'});
ylabel('Time [s]');
saveas(gca,'plots/time.png');

%time ev
clf;
for i=1:size(total_tictoc, 2)
    y = cell2mat(total_tictoc(i));
    b = bar(1:size(y,2), y);
    hold on; grid minor;
    line([0, size(y,2)+1], [0.1, 0.1], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--');
end
if plot_switch
    if LQR, xline(switch_it + 0.5, ':', {'Controller','Switch'}, 'FontSize', 9);
    else, xline(switch_it + 0.5, ':', {'Sampling','Switch'}, 'FontSize', 9); end
end
%title('Evolution of MPC iteration time');
xlabel('Iterations');
ylabel('MPC iteration time [s]');
saveas(gca, 'plots/time_ev.png');

%input
clf;
for i=1:size(total_input_traj,2)
    matrix = cell2mat(total_input_traj{i});
    u1 = matrix(1,:);
    u2 = matrix(2,:);
    u3 = matrix(3,:);
    u4 = matrix(4,:);
    x = cell2mat(total_time_traj{i});
    x_previous = 0;
    
    subplot(4, 1, 1);
    for j = 1:size(matrix,2)-1
        line([x_previous, x(j)], [u1(j), u1(j)], 'Color',[1, 0, 1, 1], 'LineWidth', 1);
        line([x(j), x(j)], [u1(j), u1(j+1)], 'Color',[1, 0, 1, 1], 'LineWidth', 1);
        x_previous = x(j);
    end
    line([0, x(end)], [-planner.lim(1,3), -planner.lim(1,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(1,3), planner.lim(1,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 5);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 5); end
    end
    grid minor;
    %title('Input evolutions on wheels and arm joints');
    ylabel('\fontsize{12}{0}$\tau_{l}$ \fontsize{10}{0}$ \textsf{[N$\cdot$m]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(planner.lim(1,3) + planner.lim(1,3)/10) planner.lim(1,3) + planner.lim(1,3)/10]);
    x_previous = 0;
    
    subplot(4, 1, 2);
    for j = 1:size(matrix,2)-1
        line([x_previous, x(j)], [u2(j), u2(j)], 'Color',[0, 1, 0, 1], 'LineWidth', 1);
        line([x(j), x(j)], [u2(j), u2(j+1)], 'Color',[0, 1, 0, 1], 'LineWidth', 1);
        x_previous = x(j);
    end
    line([0, x(end)], [-planner.lim(2,3), -planner.lim(2,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(2,3), planner.lim(2,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 5);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 5); end
    end
    grid minor;
    ylabel('\fontsize{12}{0}$\tau_{r}$ \fontsize{10}{0}$ \textsf{[N$\cdot$m]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(planner.lim(2,3) + planner.lim(2,3)/10) planner.lim(2,3) + planner.lim(2,3)/10]);
    x_previous = 0;
    
    subplot(4, 1, 3);
    for j = 1:size(matrix,2)-1
        line([x_previous, x(j)], [u3(j), u3(j)], 'Color',[0, 0, 1, 1], 'LineWidth', 1);
        line([x(j), x(j)], [u3(j), u3(j+1)], 'Color',[0, 0, 1, 1], 'LineWidth', 1);
        x_previous = x(j);
    end
    line([0, x(end)], [-planner.lim(3,3), -planner.lim(3,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(3,3), planner.lim(3,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 5);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 5); end
    end
    grid minor;
    ylabel('\fontsize{12}{0}$\tau_{1}$ \fontsize{10}{0}$ \textsf{[N$\cdot$m]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(planner.lim(3,3) + planner.lim(3,3)/10) planner.lim(3,3) + planner.lim(3,3)/10]);
    x_previous = 0;
    
    subplot(4, 1, 4);
    for j = 1:size(matrix,2)-1
        line([x_previous, x(j)], [u4(j), u4(j)], 'Color',[0.9290 0.6940 0.1250, 1], 'LineWidth', 1);
        line([x(j), x(j)], [u4(j), u4(j+1)], 'Color',[0.9290 0.6940 0.1250, 1], 'LineWidth', 1);
        x_previous = x(j);
    end
    line([0, x(end)], [-planner.lim(4,3), -planner.lim(4,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(4,3), planner.lim(4,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 5);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 5); end
    end
    grid minor;
    ylabel('\fontsize{12}{0}$\tau_{2}$ \fontsize{10}{0}$ \textsf{[N$\cdot$m]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(planner.lim(4,3) + planner.lim(4,3)/10) planner.lim(4,3) + planner.lim(4,3)/10]);
    
end
saveas(gca,'plots/input.png');

%vector q
clf;
for i=1:size(total_q_traj,2)
    matrix = cell2mat(total_q_traj{i});
    q1 = matrix(1,:);
    q2 = matrix(2,:);
    q3 = matrix(3,:);
    q4 = matrix(4,:);
    q5 = matrix(5,:);
    x = cell2mat(total_time_traj{i});
    %sgtitle('Evolutions of q components');
    
    subplot(3, 2, 1);
    grid minor;
    line(x, q1, 'Color', "#FF6347", 'LineWidth', 1);
    ylabel('\fontsize{12}{0}$x_{F}$ \fontsize{10}{0}$ \textsf{[m]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end
    
    subplot(3, 2, 2);
    grid minor;
    line(x, q2, 'Color', "#FF8C00", 'LineWidth', 1);
    ylabel('\fontsize{12}{0}$y_{F}$ \fontsize{10}{0}$ \textsf{[m]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end
    
    subplot(3, 2, 3);
    grid minor;
    line(x, q3, 'Color', "#3CB371", 'LineWidth', 1);
    ylabel('\fontsize{12}{0}$\phi$ \fontsize{10}{0}$ \textsf{[rad]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end

    subplot(3, 2, 5);
    grid minor;
    line(x, q4, 'Color', "#6495ED", 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(3,1), -planner.lim(3,1)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(3,1), planner.lim(3,1)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    ylabel('\fontsize{12}{0}$\theta_{1}$ \fontsize{10}{0}$ \textsf{[rad]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(planner.lim(3,1) + planner.lim(3,1)/10) planner.lim(3,1) + planner.lim(3,1)/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end
    
    subplot(3, 2, 6);
    grid minor;
    line(x, q5, 'Color', "#8A2BE2", 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(4,1), -planner.lim(4,1)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(4,1), planner.lim(4,1)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    ylabel('\fontsize{12}{0}$\theta_{2}$ \fontsize{10}{0}$ \textsf{[rad]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(planner.lim(4,1) + planner.lim(4,1)/10) planner.lim(4,1) + planner.lim(4,1)/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end

end
saveas(gca,'plots/q.png');

%vector q_d
clf;
for i=1:size(total_q_d_traj,2)
    matrix = cell2mat(total_q_d_traj{i});
    q1_d = matrix(1,:);
    q2_d = matrix(2,:);
    q3_d = matrix(3,:);
    q4_d = matrix(4,:);
    q5_d = matrix(5,:);
    x = cell2mat(total_time_traj{i});
    %sgtitle('Evolutions of q derivatives');
    
    subplot(3, 2, 1);
    grid minor;
    line(x, q1_d, 'Color', "#FF6347", 'LineWidth', 1);
    ylabel('\fontsize{12}{0}$\dot{x}_{F}$ \fontsize{10}{0}$ \textsf{[m /s]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end
    
    subplot(3, 2, 2);
    grid minor;
    line(x, q2_d, 'Color', "#FF8C00", 'LineWidth', 1);
    ylabel('\fontsize{12}{0}$\dot{y}_{F}$ \fontsize{10}{0}$ \textsf{[m /s]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end
    
    subplot(3, 2, 3);
    grid minor;
    line(x, q3_d, 'Color', "#3CB371", 'LineWidth', 1);
    ylabel('\fontsize{12}{0}$\dot{\phi}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end
    
    subplot(3, 2, 5);
    grid minor;
    line(x, q4_d, 'Color', "#6495ED", 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(3,2), -planner.lim(3,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(3,2), planner.lim(3,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    ylabel('\fontsize{12}{0}$\dot{\theta}_{1}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(planner.lim(3,2) + planner.lim(3,2)/10) planner.lim(3,2) + planner.lim(3,2)/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end
    
    subplot(3, 2, 6);
    grid minor;
    line(x, q5_d, 'Color', "#8A2BE2", 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(4,2), -planner.lim(4,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(4,2), planner.lim(4,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    ylabel('\fontsize{12}{0}$\dot{\theta}_{2}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(planner.lim(4,2) + planner.lim(4,2)/10) planner.lim(4,2) + planner.lim(4,2)/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 7);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 7); end
    end

end
saveas(gca,'plots/q_derivatives.png');

%vector v
clf;
for i=1:size(total_v_traj,2)
    matrix = cell2mat(total_v_traj{i});
    v1 = matrix(1,:);
    v2 = matrix(2,:);
    v3 = matrix(3,:);
    v4 = matrix(4,:);
    x = cell2mat(total_time_traj{i});
    
    subplot(4, 1, 1);
    grid minor;
    line(x, v1, 'Color',[1, 0, 1, 1], 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(1,2), -planner.lim(1,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(1,2), planner.lim(1,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    %title('Velocity evolutions on wheels and arm joints');
    xlabel('Time [s]');
    ylabel('\fontsize{12}{0}$\dot{\theta}_{l}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
    ylim([-(planner.lim(1,2) + planner.lim(1,2)/10) planner.lim(1,2) + planner.lim(1,2)/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 5);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 5); end
    end

    subplot(4, 1, 2);
    grid minor;
    line(x, v2, 'Color',[0, 1, 0, 1], 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(2,2), -planner.lim(2,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(2,2), planner.lim(2,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    xlabel('Time [s]');
    ylabel('\fontsize{12}{0}$\dot{\theta}_{r}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
    ylim([-(planner.lim(2,2) + planner.lim(2,2)/10) planner.lim(2,2) + planner.lim(2,2)/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 5);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 5); end
    end

    subplot(4, 1, 3);
    grid minor;
    line(x, v3, 'Color',[0, 0, 1, 1], 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(3,2), -planner.lim(3,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(3,2), planner.lim(3,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    xlabel('Time [s]');
    ylabel('\fontsize{12}{0}$\dot{\theta}_{1}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
    ylim([-(planner.lim(3,2) + planner.lim(3,2)/10) planner.lim(3,2) + planner.lim(3,2)/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 5);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 5); end
    end

    subplot(4, 1, 4);
    grid minor;
    line(x, v4, 'Color',[0.9290 0.6940 0.1250, 1], 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(4,2), -planner.lim(4,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(4,2), planner.lim(4,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    xlabel('Time [s]');
    ylabel('\fontsize{12}{0}$\dot{\theta}_{2}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
    ylim([-(planner.lim(4,2) + planner.lim(4,2)/10) planner.lim(4,2) + planner.lim(4,2)/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 5);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 5); end
    end
    
end
saveas(gca,'plots/velocity_v.png');

%EE velocity
clf;
for i=1:size(total_EEv_traj,2)
    matrix = cell2mat(total_EEv_traj{i});
    xe = matrix(1,:);
    ye = matrix(2,:);
    x = cell2mat(total_time_traj{i});
    subplot(2, 1, 1);
    grid minor;
    line(x, xe, 'Color',[1, 0, 1, 1], 'LineWidth', 1);
    %title('EE velocity evolutions on x and y axes');
    ylabel('\fontsize{12}{0}$\dot{x}_{E}$ \fontsize{10}{0}$ \textsf{[m /s]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 8);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 8); end
    end
    
    subplot(2, 1, 2);
    grid minor;
    line(x, ye, 'Color',[0.9290 0.6940 0.1250, 1], 'LineWidth', 1);
    ylabel('\fontsize{12}{0}$\dot{y}_{E}$ \fontsize{10}{0}$ \textsf{[m /s]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 8);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 8); end
    end
    
end
saveas(gca,'plots/velocity_EE.png');

%driving and sterring velocities
clf;
kinematic_parameters;
b = evalin('base',"b");
r = evalin('base',"r");
driving_vel_lim = 0.5;
steering_vel_lim = 3.35;
for i=1:size(total_v_traj,2)
    matrix = cell2mat(total_v_traj{i});
    v1 = matrix(1,:);
    v2 = matrix(2,:);
    x = cell2mat(total_time_traj{i});
    subplot(2, 1, 1);
    grid minor;
    line(x, r*(v2+v1)/2, 'Color',[1, 0.375, 0, 1], 'LineWidth', 1);
    line([0, x(end)], [-driving_vel_lim, -driving_vel_lim], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [driving_vel_lim, driving_vel_lim], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    %title('Driving and steering velocities evolutions');
    ylabel('\fontsize{12}{0}$v_{dr}$ \fontsize{10}{0}$ \textsf{[m /s]}$', 'Interpreter','latex');
    ylim([-(driving_vel_lim + driving_vel_lim/10) driving_vel_lim + driving_vel_lim/10]);
    xlabel('Time [s]');
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 8);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 8); end
    end
    
    subplot(2, 1, 2);
    grid minor;
    line(x, r*(v2-v1)/b, 'Color',[0.0 0.719 1, 1], 'LineWidth', 1);
    line([0, x(end)], [steering_vel_lim, steering_vel_lim], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [-steering_vel_lim, -steering_vel_lim], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    ylabel('\fontsize{12}{0}$w_{st}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
    xlabel('Time [s]');
    ylim([-(steering_vel_lim + steering_vel_lim/10) steering_vel_lim + steering_vel_lim/10]);
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 8);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 8); end
    end
end
saveas(gca,'plots/ds_velocity.png');

%distance goal
clf;
for i=1:size(total_distance_goal,2)
    y = cell2mat(total_distance_goal(i));
    x = cell2mat(total_time_traj{i});
    grid minor;
    line(x, y, 'Color',[0, 0, 1, 0.3], 'LineWidth', 1);
    ylabel('Distance [m]');
    xlabel('Time [s]');
end
if plot_switch
    if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 9);
    else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 9); end
end
%title('Evolution of distance from goal');
saveas(gca, 'plots/distance_goal.png');

%distance goal wrt iterations
clf;
for i=1:size(total_distance_goal_it,2)
    y = cell2mat(total_distance_goal_it(i));
    x = 1:size(cell2mat(total_distance_goal_it(i)), 2);
    grid minor;
    line(x, y, 'Color', [0, 0, 1, 0.3], 'LineWidth', 1);
    ylabel('Distance [m]');
    xlabel('Iterations');
end
if plot_switch
    if LQR, xline(switch_it + 0.5, ':', {'Controller','Switch'}, 'FontSize', 9);
    else, xline(switch_it + 0.5, ':', {'Sampling','Switch'}, 'FontSize', 9); end
end
%title('Evolution of distance from goal');
saveas(gca, 'plots/distance_goal_it.png');

%distance obs wrt time 
if ~isempty(planner.w_model.obstacles)
    clf;
    for i=1:size(total_distance_obs,2)
        y = cell2mat(total_distance_obs(i));
        x = cell2mat(total_time_traj{i});
        grid minor;
        line(x, y, 'Color', [0, 0, 1, 0.3], 'LineWidth', 1);
        ylabel('Distance [m]');
        xlabel('Time [s]');
    end
    if plot_switch
        if LQR, xline(switch_t, ':', {'Controller','Switch'}, 'FontSize', 9);
    	else, xline(switch_t, ':', {'Sampling','Switch'}, 'FontSize', 9); end
    end
    %title('Evolution of distance from obstacles');
    saveas(gca, 'plots/distance_obs.png');
end

%distance obs wrt iterations
if ~isempty(planner.w_model.obstacles)
    clf;
    for i=1:size(total_distance_obs_it,2)
        y = cell2mat(total_distance_obs_it(i));
        x = 1:size(cell2mat(total_distance_obs_it(i)), 2);
        grid minor;
        line(x, y, 'Color', [0, 0, 1, 0.3], 'LineWidth', 1);
        ylabel('Distance [m]');
        xlabel('Iterations');
    end
    if plot_switch
        if LQR, xline(switch_it + 0.5, ':', {'Controller','Switch'}, 'FontSize', 9);
    	else, xline(switch_it + 0.5, ':', {'Sampling','Switch'}, 'FontSize', 9); end
    end
    %title('Evolution of distance from obstacles');
    saveas(gca, 'plots/distance_obs_it.png');
end

%trajectory EE 
clf;
daspect([1, 1, 1]);
xlim([-1, goal(1)+1]);
ylim([-1, goal(2)+1]);
grid minor;
circles(goal(1), goal(2), 0.2, 'facecolor', 'green','facealpha',0.5);
for i = 1:size(planner.w_model.obstacles,2)
    ob = planner.w_model.obstacles{i};
    circles(ob(1), ob(2), ob(3), 'facecolor', 'red');
end
line([0.57, goal(1)], [0, goal(2)], ... 
    'LineWidth', 0.05, ...
    'Color','red', ...
    'LineStyle','--');
drawRobot([0;0;0;0;0;]);
drawRobot(planner.v_model.q);
for i=1:size(total_traj,2)
    matrix = cell2mat(total_traj{i});
    x = matrix(1,:);
    y = matrix(2,:);
    line(x,y, 'Color',[0, 0, 1, 1], 'LineWidth', 1);
end
%title('EE trajectory');
ylabel('y axis [m]');
xlabel('x axis [m]');
saveas(gcf, 'plots/trajectoryEE.png');

%trajectory COM
clf;
daspect([1, 1, 1]);
xlim([-1, goal(1)+1]);
ylim([-1, goal(2)+1]);
grid minor;
circles(goal(1), goal(2), 0.2, 'facecolor', 'green','facealpha',0.5);
for i = 1:size(planner.w_model.obstacles,2)
    ob = planner.w_model.obstacles{i};
    circles(ob(1), ob(2), ob(3), 'facecolor', 'red');
end
line([0.57, goal(1)], [0, goal(2)], ... 
    'LineWidth', 0.05, ...
    'Color','red', ...
    'LineStyle','--');
drawRobot([0;0;0;0;0;]);
drawRobot(planner.v_model.q);
for i=1:size(total_base_traj,2)
    matrix = cell2mat(total_base_traj{i});
    x = matrix(1,:);
    y = matrix(2,:);
    line(x,y, 'Color',[0.4940, 0.1840, 0.5560, 1], 'LineWidth', 1);
end
%title('COM base trajectory');
ylabel('y axis [m]');
xlabel('x axis [m]');
saveas(gcf, 'plots/trajectoryCOM.png');

%trajectory EE + COM
clf;
daspect([1, 1, 1]);
xlim([-1, goal(1)+1]);
ylim([-1, goal(2)+1]);
legStr = {};
grid minor;
circles(goal(1), goal(2), 0.2, 'facecolor', 'green','facealpha',0.5, 'HandleVisibility', 'off');
for i = 1:size(planner.w_model.obstacles,2)
    ob = planner.w_model.obstacles{i};
    circles(ob(1), ob(2), ob(3), 'facecolor', 'red', 'HandleVisibility', 'off');
end
line([0.57, goal(1)], [0, goal(2)], ... 
    'LineWidth', 0.05, ...
    'Color','red', ...
    'LineStyle','--', 'HandleVisibility','off');
drawRobot([0;0;0;0;0;]);
drawRobot(planner.v_model.q);
for i=1:size(total_base_traj,2)
    matrix = cell2mat(total_base_traj{i});
    x = matrix(1,:);
    y = matrix(2,:);
    line(x,y, 'Color',[0.4940, 0.1840, 0.5560, 1], 'LineWidth', 1);
    legStr = [legStr; "COM base trajectory"];
    matrix = cell2mat(total_traj{i});
    x = matrix(1,:);
    y = matrix(2,:);
    line(x,y, 'Color',[0, 0, 1, 1], 'LineWidth', 1);
    legStr = [legStr; "EE trajectory"];
    leg = legend(legStr, 'location','northwest');
end
%title('EE and COM base trajectory');
ylabel('y axis [m]');
xlabel('x axis [m]');
saveas(gcf, 'plots/trajectoryEE_COM.png');

function drawRobot(q)
    x_0 = q(1);
    y_0 = q(2);

    l1 = evalin('base', "l1");
    l2 = evalin('base', "l2");

    x_1 = x_0 + l1 * cos(q(4) + q(3));
    y_1 = y_0 + l1 * sin(q(4) + q(3));
    x_2 = x_1 + l2 * cos(q(4) + q(5) + q(3));
    y_2 = y_1 + l2 * sin(q(4) + q(5) + q(3));

    ver_0 = [ [-0.6, -0.15, 1];
              [   0, -0.15, 1];
              [   0,  0.15, 1];
              [-0.6,  0.15, 1]];

    T = [ [cos(q(3)),  -sin(q(3)),  x_0];
          [sin(q(3)),   cos(q(3)),  y_0];
          [        0,           0,    1]];

    ver_w = T * ver_0';

    patch(ver_w(1,:), ver_w(2,:), 'black', 'HandleVisibility', 'off');
    line([x_0, x_1], [y_0, y_1], 'LineWidth', 2, 'HandleVisibility', 'off');
    line([x_1, x_2], [y_1, y_2], 'LineWidth', 2, 'HandleVisibility', 'off');
end