%goal: point desidered to reach [xe, ye, vxe, xye]
goal            = [4; 4; 0; 0];

%state of the world == obstacles
world = WorldModel();
world_state = [];
ray = 0.3;
new_obs = [0, 0, ray, 0, 0];
i = 0;
while i < 12
    add = true;
    new_obs(1:2) = (goal(1:2) + 0.3) .* rand(2, 1);
    if ((norm(new_obs(1:2)) <= 1 + ray) || (norm(goal(1:2) - new_obs(1:2)) <= 1 + ray))
        add = false;
        continue;
    end
    for j = 1:5:size(world_state, 2)
        if (norm(world_state(j:j+1) - new_obs(1:2)) <= (2 * ray + rand()) )
            add = false;
            break;
        end
    end
    if add 
        world_state = [world_state, new_obs];
        i = i +1;
    end
end
display(world_state);
world.UpdateState(world_state);

world_dynamic = false;