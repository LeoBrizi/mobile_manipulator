%goal: point desidered to reach [xe, ye, vxe, xye]
goal            = [4; 4; 0; 0];

%state of the world == obstacles
world = WorldModel();
world_state = [[2, 1, 0.5, 0, 0], [2, 0, 0.5, 0, 0], [1.8, 3, 0.5, 0, 0], [0.5, 3, 0.5, 0, 0], [3, 3, 0.5, 0, 0]];
world.UpdateState(world_state);

world_dynamic = false;