%goal: point desidered to reach [xe, ye, vxe, xye]
goal            = [4; 4; 0; 0];

%state of the world == obstacles
world = WorldModel();
world_state = [[1.2, 1.2, 0.3, 0, 0], [2.2, 1.2, 0.3, 0, 0], [1.2, 2.2, 0.3 , 0, 0], [3, 3, 0.3 , 0, 0],...
    [0.5, 5, 0.5, 0, 0], [1.5, 5, 0.5, 0, 0], [2.5, 5, 0.5, 0, 0], [5, 0.5, 0.5, 0, 0], [5, 1.5, 0.5, 0, 0], [5, 2.5, 0.5, 0, 0]];
world.UpdateState(world_state);

world_dynamic = false;