%goal: point desidered to reach [xe, ye, vxe, xye]
goal            = [7; 7; 0; 0];

%state of the world == obstacles
world = WorldModel();
world_state = [[2, 0, 0.3, 0.02, 0.05], [0, 1.5, 0.3, 0.05, 0.02], [0, 3.5, 0.3, 0.1, 0], [4, 0, 0.3, 0, 0.1], [6,6,0.3,-0.05,-0.05], [6, 8, 0.3, 0, -0.07], [8, 6, 0.3, -0.07, 0],...
                [6, 0, 0.3, 0, 0.05], [6, 0.5, 0.3, 0, 0.05], [6, 1, 0.3, 0, 0.05], [6, 1.5, 0.3, 0, 0.05]];
world.UpdateState(world_state);

world_dynamic = true;
