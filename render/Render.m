classdef Render

    properties
        model;
        goal;
        world;
    end
    
    methods
 
        function obj = Render (model, world, goal)
            daspect([1, 1, 1]);
            obj.model = model;
            obj.world = world;
            obj.goal = goal;
            xlim([-1, obj.goal(1)+1]);
            ylim([-1, obj.goal(2)+1]);
        end

        function Draw(obj)
            cla;
            circles(obj.goal(1), obj.goal(2), 0.1, 'facecolor', 'green');
            
            obstacles = obj.world.GetState();
            for i = 1:size(obstacles,2)
                ob = obstacles{i};
                circles(ob(1), ob(2), ob(3), 'facecolor', 'red');
            end
            q = obj.model.q;
            x_0 = q(1);
            y_0 = q(2);
            
            l1 = evalin('base', "l1");
            l2 = evalin('base', "l2");
            
            x_1 = x_0 + l1 * cos(q(4) + q(3));
            y_1 = y_0 + l1 * sin(q(4) + q(3));
            x_2 = x_1 + l2 * cos(q(4) + q(5) + q(3));
            y_2 = y_1 + l2 * sin(q(4) + q(5) + q(3));
            
            ver_0 = [ [-0.6, -0.15, 1];
                      [   0, -0.15, 1];
                      [   0,  0.15, 1];
                      [-0.6,  0.15, 1]];
                  
            T = [ [cos(q(3)),  -sin(q(3)),  x_0];
                  [sin(q(3)),   cos(q(3)),  y_0];
                  [        0,           0,    1]];

            ver_w = T * ver_0';

            patch(ver_w(1,:), ver_w(2,:), 'black');
            line([x_0, x_1], [y_0, y_1], 'LineWidth', 2);
            line([x_1, x_2], [y_1, y_2], 'LineWidth', 2);
            
            % plot circles around robot parts (i.e. base and arm links)
            radii = obj.model.GetRadii();
            centers_rob = obj.model.GetCenters();
            circles(centers_rob(1,1), centers_rob(2,1), radii(1), 'facecolor', 'none');
            circles(centers_rob(1,2), centers_rob(2,2), radii(2), 'facecolor', 'none');
            circles(centers_rob(1,3), centers_rob(2,3), radii(3), 'facecolor', 'none');
            
            % show area covered by sensors
            circles(q(1), q(2), 3, 'facecolor', 'none', 'edgecolor', 'b', 'linestyle', ':');
            
            if ~isempty(obstacles)
                [cc,no,nc] = obj.GetCircleClearance();
                dir = ([no(1);no(2)]-nc(1:2))/norm([no(1);no(2)]-nc(1:2));
                if(cc<=0.2)
                    line([nc(1)+(nc(3)*dir(1)), no(1)], [nc(2)+(nc(3)*dir(2)), no(2)], 'LineWidth', 1, 'Color','red');
                end
            end
            
        end
        
        function [cc,no,nc] = GetCircleClearance(obj)
            centers_rob = obj.model.GetCenters();
            radii_rob = obj.model.GetRadii();
            obstacles = obj.world.GetState();
            %disp(obstacles);
            
            cc = Inf;
            for i=1:size(obstacles,2)
                obstacle = obstacles{i};
                center_obs = [obstacle(1);obstacle(2)];
                radius_obs = obstacle(3);
                for j=1:size(centers_rob,2)
                    curr_cc = norm(center_obs - centers_rob(:,j))-radius_obs-radii_rob(j);
                    if(curr_cc < cc)
                        cc = curr_cc; 
                        no = obstacle;
                        nc = [centers_rob(:,j);radii_rob(j)];
                    end
                end
                
            end        
        end
        
        function DrawTree(~, tree, unpruned, ID_best, v_model)
            path = tree.ExtractPath(ID_best, 1);

            for i = 1:tree.GetLength()
                data = tree.GetData(i);

                direct_kin = v_model.DirectKinematic(data{1});
                position = direct_kin(1:2);
                
                if ismember(i, path) && ismember(i, unpruned)
                    circles(position(1), position(2), 0.01, 'facecolor', 'red');
                elseif ismember(i, unpruned)
                    circles(position(1), position(2), 0.01, 'facecolor', 'blue');
                else 
                    circles(position(1), position(2), 0.01, 'facecolor', '#A9A9A9');
                end

                if ~tree.IsLeaf(i)
                    children_IDs = tree.GetChildren(i);
                    for j = children_IDs
                        child_data = tree.GetData(j);

                        direct_kin = v_model.DirectKinematic(child_data{1});
                        child_position = direct_kin(1:2);
                        
                        if ismember(j, path) && ismember(j, unpruned)
                            line([position(1), child_position(1)], [position(2), child_position(2)],'LineWidth', 0.5, 'Color', 'red');
                        elseif ismember(j, unpruned)
                            line([position(1), child_position(1)], [position(2), child_position(2)],'LineWidth', 0.5, 'Color', 'blue');  
                        else
                            line([position(1), child_position(1)], [position(2), child_position(2)],'LineWidth', 0.5, 'Color', '#A9A9A9');
                        end
                    end
                end
            end
        end
    end
end
