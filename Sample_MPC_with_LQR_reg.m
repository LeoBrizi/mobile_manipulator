addpath common;
addpath dynamic_model;
addpath world_model;
addpath motion_planner;
addpath render;
addpath regulation;
addpath simulation_evn;
LQR = true;

dynamic_parameters;
kinematic_parameters;

test_random;

simulation_iteration = 1200;

%parameters for the planner
%delta_t is the static time step for the planner
%h is the time horizon of prediction
%N is the number of the tree expansion done by the algorithm
%pruning is a flag to enable the pruning in the algorithm
%heuristic is a flag to enable a heuristic for finding good trajectories
%we, wb, wt, are weights for the cost function in order
%we: weight for error in end_effector positioning
%wb: weight for error in base positioning
%wt: weight for counting the effect of big inputs
%k: how many of the predicted input to apply to real robot

delta_t         = 0.10;
h               = 5.5;    %3;         %5 with obstacle
N               = 150;  %400;     %300
heuristic       = true;

we          = 300;
wu          = 1;
weev        = 300;
wbv         = 3500;%3000 for the step cost function
w_cclear    = 200;
cclear_min  = 2.5;
distance_to_switch = 0;

k         = 1;
plot_flag = false;
plot_tree = false;
plot_switch = true;

iteration = 1;

%model of the real robot
real_robot = DynamicModel();


%planner = Planner(real_robot.GetState(), world, delta_t, h, N, we, wu, weev, wbv, w_cclear, cclear_min, distance_to_switch);
planner = PlannerHard(real_robot.GetState(), world, delta_t, h, N, we, wu, weev, wbv, distance_to_switch);
planner.SetGoal(goal);

%stats for plots
tic_toc = [];
input_traj = {};
q_traj = {};
q_d_traj = {};
v_traj = {};
EEv_traj = {};
time_traj = {};
distance_goal = [];
distance_goal_it = [];
distance_obs = [];
distance_obs_it = [];
trajectory = {};
base_trajectory= {};
time = 0;

if plot_flag, render = Render(real_robot, world, goal(1:2)); end
EE_err = Inf;
distance_to_lqr = 0.02;
velocity_to_lqr = 0.1;

while EE_err > distance_to_lqr || norm(real_robot.v) > velocity_to_lqr %norm(diff_direct_kin(3:4)) > 0.05 || norm(diff_direct_kin(1:2)) > 0.05
    fprintf('Iteration: %i--------------------------------\n', iteration);
    elapse_time = tic();
    [U_traj, t_traj] = planner.ControlTrajectory();
    time_to_decision = toc(elapse_time);
    tic_toc(end+1) = time_to_decision;
    distance_obs_it(end+1) = planner.GetCircleClearance;
    
    direct_kin = real_robot.DirectKinematic(real_robot.q);
    EE_err = norm(direct_kin(1:2) - goal(1:2));
    distance_goal_it(end+1) = EE_err;
    if planner.ID_best ~= 1
%         if plot_flag
%             render = Render(real_robot, world, goal(1:2));
%             hold on;
%             render.Draw();
%             if plot_tree
%                 render.DrawTree(planner.tree, planner.unpruned, planner.ID_best, planner.v_model);
%             end
%             drawnow();
%         end
        %path found apply first k control step
        for i = 1:k
            if (i > size(U_traj, 2)); break, end
            
            real_robot.Step(U_traj(:, i), t_traj(i));
            if plot_flag
                hold on;
                render.Draw();
                drawnow();
            end
            if world_dynamic
                world.Step(t_traj(i));
            end
            %if plot_flag,render.Draw(); end
            
            direct_kin = real_robot.DirectKinematic(real_robot.q);
            diff_direct_kin = real_robot.DifferentialDirectKinematic(real_robot.q, real_robot.v);
            time = time + t_traj(i);
             
            EE_err = norm(direct_kin(1:2) - goal(1:2));
            velocity_err = norm(diff_direct_kin(1:2)- goal(3:4));
            fprintf('end effector error: %.3f velocity error: %.3f\n', EE_err, velocity_err);
            input_traj{end+1} = U_traj(:,i);
            q_traj{end+1} = real_robot.q;
            q_d_traj{end+1} = real_robot.q_d;
            v_traj{end+1} = real_robot.v;
            EEv_traj{end+1} = diff_direct_kin(1:2);
            time_traj{end+1} = time;
            trajectory{end+1} = direct_kin(1:2);
            base_trajectory{end+1} = direct_kin(3:4) - lg * [cos(real_robot.q(3)); sin(real_robot.q(3))];
            distance_goal(end+1) = EE_err;
            distance_obs(end+1) = planner.GetCircleClearance;
        end
    end
    planner.Reset(real_robot.GetState(), heuristic, U_traj(:, k+1:end), t_traj(k+1:end));
    iteration = iteration + 1;
end

switch_it = iteration;
switch_t = time;

real_robot.GetState()
display("last position")
direct_kin = real_robot.DirectKinematic(real_robot.q)

display("change controller :D");

goal_q_space = real_robot.InverseKinematicEE(goal(1:2), 1e-4, 1e-8);
goal_q_space = [goal_q_space; 0.001; 0.001; 0.001; 0.001];

last_input = [0;0;0;0];%input_traj{end};

K = Regularize(real_robot, goal_q_space, last_input);

time_input = 0.1; %it has to be low for linearity

for i = 1:1000
    elapse_time = tic();
    input = -K*(real_robot.GetState() - goal_q_space);
    %display(norm(real_robot.GetState() - goal_q_space));
    time_to_decision = toc(elapse_time);
    tic_toc(end+1) = time_to_decision;
     
    real_robot.Step(input, time_input);
    
    direct_kin = real_robot.DirectKinematic(real_robot.q);
    diff_direct_kin = real_robot.DifferentialDirectKinematic(real_robot.q, real_robot.v);
    
    EE_err = norm(direct_kin(1:2) - goal(1:2));
    velocity_err = norm(diff_direct_kin(1:2)- goal(3:4));
    fprintf('end effector error: %.3f velocity error: %.3f\n', EE_err, velocity_err);
    
    time = time + time_input;
    distance_obs_it(end+1) = planner.GetCircleClearance;
    distance_goal_it(end+1) = EE_err;
    input_traj{end+1} = input;
    q_traj{end+1} = real_robot.q;
    q_d_traj{end+1} = real_robot.q_d;
    v_traj{end+1} = real_robot.v;
    EEv_traj{end+1} = diff_direct_kin(1:2);
    time_traj{end+1} = time;
    trajectory{end+1} = direct_kin(1:2);
    base_trajectory{end+1} = direct_kin(3:4) - lg * [cos(real_robot.q(3)); sin(real_robot.q(3))];
    distance_goal(end+1) = EE_err;
    distance_obs(end+1) = planner.GetCircleClearance;

    if plot_flag
        hold on;
        render.Draw();
        drawnow();
    end
end

real_robot.GetState()
display("final position")
direct_kin = real_robot.DirectKinematic(real_robot.q)

