addpath ./vrep_lib;
addpath ./dynamic_model;

%import DynamicModel;
%import VrepConnectorNoDym;

dynamic_parameters;
kinematic_parameters;

model = DynamicModel();
vrep = VrepConnectorNoDym(model, 9009);
for i = 1:200
    u = [2;1;0;0];
    model.Step(u, 0.2);
    model.q
    vrep.RefreshSim();
    %pause(1/20);
end

vrep.Close();
