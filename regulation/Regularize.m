function K = Regularize(real_robot, x, u_k)
    [A, B] = real_robot.LinearizeDynamic(x, u_k);
    display("rank of the controllability matrix:");
    rank(ctrb(A,B))
    Q = 1 * eye(9,9);
%     Q(9,9) = 1; 
%     Q(8,8) = 1;
%     Q(7,7) = 100;
%     Q(6,6) = 100;
    R = 100*eye(4,4);
    
    [K, S, e] = lqr(A, B, Q, R);  
end