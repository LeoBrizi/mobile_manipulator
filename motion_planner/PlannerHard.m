classdef PlannerHard < handle
    % Planner which implement randomised MPC-based algorithm
    %   as described in the paper "Randomised MPC-based Motion-Planning for Mobile Robot Obstacle Avoidance"
    
    properties
        tree;           % current tree expansion where each node is (state, time, input)
        c_min;          % current minimum cost of a path in the tree
        ID_best;        % id of the leaf of the minimum cost path
        delta_t;    
        h;              % time horizon of predition 
        N;              % number of tree expansion
        v_model;        % model of the robot
        w_model;
        obstacles;      % obstacles of the world
        goal;           % desidered position and velocity of end effector 
        %these variance are on funtion of distance from the goal
        % variance of the gaussian for input sampling
        var_max = 0.05; 
        var_min = 0.0001;
        distance_to_switch;
        velocity_to_switch = 0.05;
        distance_to_regularize = 2;

        %coefficients of cost function 
        Wee;         % weight for end_effector position
        Wu;         % weight for taking in account of input
        weev;       % weight for end_effector velocity
        wbv;
        w_cclear;
        cclear_min;

        unpruned;   % which node are unpruned
        
        % joints limits (position, velocity, torque) 
        lim = [[0,      4.1667,  0.2]; % change limits
               [0,      4.1667,  0.2];
               [3/4*pi,      1,  0.1]; 
               [3/4*pi,      1,  0.1]];

        % radii for obstacle avoidance
        r; % [base, arm 1, arm 2]
        
    end
    
    methods
        function obj = PlannerHard(v_initial_state, world_model, delta_t, h, N, we, wu, weev, wbv, distance_to_switch)
            % Costructor of the class
            %   initialize the state of the robot model and set all the parameters
            obj.v_model = DynamicModel();
            obj.w_model = world_model;
            obj.obstacles = world_model.GetLocalState(v_initial_state(1:2));
            obj.tree = Tree();
            obj.tree.AddNode(1, {v_initial_state, 0, [0; 0; 0; 0], 0});
            obj.c_min = inf;
            obj.ID_best = 1;
            obj.delta_t = delta_t;
            obj.h = h;
            obj.N = N;
            
            obj.Wee = we * eye(2,2);
            obj.Wu = wu * eye(4,4);
            obj.weev = weev;
            obj.wbv = wbv;
            
            obj.distance_to_switch = distance_to_switch;

            obj.unpruned = [1];
            
            obj.r = obj.v_model.GetRadii();
        end
        
        function Reset(obj, state, heuristic, U_traj_old, delta_t_old)
            % Reset the planner in order to replan from the next state
            obj.tree = Tree();
            parent = obj.tree.AddNode(1, {state, 0, [0; 0; 0; 0], 0});
            obj.obstacles = obj.w_model.GetLocalState(state(1:2));
            obj.c_min = inf;
            obj.ID_best = 1;
            obj.unpruned = [1];
            
            if heuristic
                % reinizialize the tree with the prevoius best branch 
                t = 0;
                obj.v_model.SetState(state, t);
                for i = 1:size(U_traj_old, 2)
                    delta_t = delta_t_old(i);
                    u = U_traj_old(:, i);
                    obj.v_model.Integrate(u, delta_t);
                    x = obj.v_model.GetState();
                    t = t + delta_t;
                    if obj.IsInfeasible(x)
                        break; 
                    end
                    c = obj.SingleCost(x, u);
                    n_new = obj.tree.AddNode(parent, {x, t, u, c});
                    obj.unpruned(end+1) = n_new;
                    parent = n_new;
                end
            end
        end
        
        function SetGoal(obj,goal)
            % set the goal for the planner
            obj.goal = goal;
        end
        
        function [U_traj, t_traj] = ControlTrajectory(obj)
            % Algorithm for the trajectory generation
            % it returns:
            %   U_traj: best input trajectory found
            %   t_traj: delta t associated to U_traj
            
            for i = 1:obj.N
                node_ID = obj.SelectNode();
                cost_est = obj.Cost(node_ID);
                if cost_est > obj.c_min  
                    current_pruned = obj.tree.Prune(node_ID);
                    obj.unpruned = setdiff(obj.unpruned, current_pruned);
                    continue;
                end
                u = obj.SelectControl(node_ID);
                data = obj.tree.GetData(node_ID);
                x = data{1};
                t = data{2};
                obj.v_model.SetState(x, t);
                parent = node_ID;
                while t < obj.h
                    delta_t = obj.SelectDeltaT();
                    obj.v_model.Integrate(u, delta_t);
                    x = obj.v_model.GetState();
                    t = t + delta_t;
                    if obj.IsInfeasible(x)
                        break; 
                    end
                    c = obj.SingleCost(x, u);
                    cost_est = cost_est + c;
                    if cost_est > obj.c_min, break; end
                    n_new = obj.tree.AddNode(parent, {x, t, u, c});
                    obj.unpruned(end+1) = n_new;
                    parent = n_new;
                    if t >= obj.h
                        %display("orizzonte raggiunto");
                        c = obj.Cost(n_new);
                        if c < obj.c_min
                            obj.c_min = c;
                            obj.ID_best = n_new;
                        end
                    end
                end
            end
            best_branch = obj.tree.ExtractPath(obj.ID_best, 1);
            % best trajectory extraction
            path_len = numel(best_branch);
            U_traj = zeros(4, path_len);
            t_traj = zeros(1, path_len);
            % dM is needed for extract delta time starting from all time stamp for each state
            dM = zeros(path_len-1, path_len);
            dM(1:path_len-1+1:end) = -1;
            dM(path_len:path_len-1+1:end) = 1;
            
            for i = 1:path_len
                data = obj.tree.GetData(best_branch(i));
                U_traj(:, i) = data{3};
                t_traj(i) = data{2};
            end
            U_traj = U_traj(:, 2:end); % remove control root
            t_traj = t_traj * dM';
        end
        
        function cc = GetCircleClearance(obj)
            centers_rob = obj.v_model.GetCenters();
            %obstacles = obj.w_model.GetState();
            
            cc = Inf;
            for i=1:size(obj.obstacles,2)
                obstacle = obj.obstacles{i};
                center_obs = [obstacle(1);obstacle(2)];
                radius_obs = obstacle(3);
                for j=1:size(centers_rob,2)
                    curr_cc = norm(center_obs - centers_rob(:,j)) - radius_obs - obj.r(j);
                    if(curr_cc < cc), cc = curr_cc; end
                end
            end        
        end
    end
            
    methods (Access = private)
        
        function infeasible = IsInfeasible(obj, x) 
            % Check if a state is feasible according to robot limit and obstacles
            q = x(1:5);
            v = x(6:9);           
            if (abs(q(4)) > obj.lim(3,1) || abs(q(5)) > obj.lim(4,1) || abs(v(3)) > obj.lim(3,2) || abs(v(4)) > obj.lim(4,2) || ... 
                abs(v(1)) > obj.lim(1,2) || abs(v(2)) > obj.lim(2,2))
                infeasible = true;
                return;
            end
            infeasible = obj.CheckCollision(q, v);            
        end
        
        function collide = CheckCollision(obj, q, v)
            collide = false;
            centers_rob = obj.v_model.GetCenters();
            %obstacles = obj.w_model.GetState();
            [driving_v, steering_v] = obj.v_model.GetBaseVelocity(v);
            phi = q(3);
            %consider the same linear velocity for all the part
            %it augment the dimension of the obstacle by a factor which
            %depends on:
            %velocity vector and distance vector from the obstacle
            velocity_vector = driving_v * [cos(phi); sin(phi)];
            
            for i = 1:size(obj.obstacles,2)
                obstacle = obj.obstacles{i};
                center_obs = obstacle(1:2);
                radius_obs = obstacle(3);
                
                for j = size(centers_rob, 2):-1:1
                    distance_vector = center_obs' - centers_rob(:,j);
                    expand_factor = (1/2) * ((dot(velocity_vector, distance_vector)^2) / 0.07);
                    %expand_factor = (dot(velocity_vector, distance_vector)^2)/2;
                    expand_factor = max(0, expand_factor);
                    if norm(distance_vector) <= radius_obs + obj.r(j) + expand_factor
                        collide = true;
                        return;
                    end
                end 
            end 
        end

        function node_ID = SelectNode(obj)
            % Select a random node from the tree
% DECOMMENT HERE FOR NODE SELECTION IN RRT STYLE
%                 %sample the space
%                 epsilon = 0.7;
%                 state = zeros(9,1);
%                 if (rand() < epsilon)
%                     state(1:2) = obj.goal(1:2).*rand(2,1);
%                     state(3) = -pi + ((2 * pi).*rand());
%                     state(4:5) = -obj.lim(3:4,1) + ((2 * obj.lim(3:4,1)).*rand(2,1));
%                     state(6:9) = -obj.lim(:,2) + ((2 * obj.lim(:,2)).*rand(4,1));
%                 else 
%                     state(1:2) = normrnd(obj.goal(1:2), 1, 2, 1);
%                     state(3) = -pi + ((2 * pi).*rand());
%                     state(4:5) = -obj.lim(3:4,1) + ((2 * obj.lim(3:4,1)).*rand(2,1));
%                     state(6:9) = [0;0;0;0];
%                     obj.v_model.SetState(state, 0);
%                     q = obj.v_model.InverseKinematicEE(obj.goal(1:2), 0.1, 0.0001);
%                     state = [q;0;0;0;0];
%                 end 
%                 node_ID = obj.tree.SelectNearestNode(state, obj.unpruned);
            i = randi([1, numel(obj.unpruned)]);
            node_ID = obj.unpruned(i);
        end
        
        function u = SelectControl(obj, node_ID)
            % Select a random input from the input space (considering constraints)
            tau_lim = obj.lim(:, 3);
            data = obj.tree.GetData(node_ID);
            state = data{1};
            direct_kin = obj.v_model.DirectKinematic(state(1:5));
            distance_to_goal = norm(direct_kin(1:2) - obj.goal(1:2));
            diff_direct_kin = obj.v_model.DifferentialDirectKinematic(state(1:5), state(6:9));
            if(distance_to_goal > obj.distance_to_switch || norm(diff_direct_kin(3:4)) > obj.velocity_to_switch)
                u = -tau_lim + ((2 * tau_lim).*rand(4, 1));
                return;
            end
            var = obj.var_max * distance_to_goal / obj.distance_to_switch;
            var = max(var, obj.var_min);
            u = normrnd(0, var, 4 , 1);
            u = min(u, tau_lim);
            u = max(u, -tau_lim);
        end
        
        function c = SingleCost(obj, x, u)
            q = x(1:5);
            v = x(6:9);
            direct_kin = obj.v_model.DirectKinematic(q);
            diff_direct_kin = obj.v_model.DifferentialDirectKinematic(q, v);
            [v_dr, w_st] = obj.v_model.GetBaseVelocity(v);
            distance_to_goal = norm(direct_kin(1:2) - obj.goal(1:2));
            
            weev = 10;
            W_drst = 1;
            W_str = 1;
            
            if distance_to_goal < obj.distance_to_regularize
                weev = obj.weev;
                W_drst = obj.wbv * abs(v_dr);
                W_str = obj.wbv * abs(w_st);
            end
            
            %weev = (obj.distance_to_regularize - min(obj.distance_to_regularize, distance_to_goal))*obj.weev;
            %d_min = 30*abs(v_dr);
            %W_drst = obj.wbv * abs(v_dr) * (0.5*sign(d_min-distance_to_goal)+0.5);
            
            Weev = weev * eye(2,2);
            Wbv = zeros(2,2);
            Wbv(1,1) = W_drst;
            Wbv(2,2) = W_str;
            
            error_end_effector = (direct_kin(1:2) - obj.goal(1:2))' * obj.Wee * (direct_kin(1:2) - obj.goal(1:2));
            error_end_effector_v = (diff_direct_kin(1:2) - obj.goal(3:4))' * Weev * (diff_direct_kin(1:2) - obj.goal(3:4));
            input_cost = u' * obj.Wu * u;
            base_vel_penalty = [v_dr, w_st] * Wbv * [v_dr; w_st];
            
            c = error_end_effector + error_end_effector_v ...
                 + input_cost + base_vel_penalty;
        end
        
        function cost = Cost(obj, node_ID)
            % Compute the cost associated to a node (depending on its parents)
            path = obj.tree.ExtractPath(node_ID, 1);
            cost = 0;
            
            for i = 1:size(path, 2)
                data = obj.tree.GetData(path(i));
                cost = cost + data{4};
            end
        end
        
        function delta_t = SelectDeltaT(obj)
            delta_t = obj.delta_t;
            return;
        end
        
    end
end
