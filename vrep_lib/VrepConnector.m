classdef VrepConnector
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        sim;
        clientID;
        base;
        motor_left;
        motor_right;
        joint1;
        joint2;
        step_time_vrep;
        l_g = 0.30;                 %add l_g of kinematic parameters of the robot
    end
    
    methods
        function obj = VrepConnector(port, step_time_vrep)
            %UNTITLED3 Construct an instance of this class
            %   Detailed explanation goes here
            obj.step_time_vrep = step_time_vrep;
            obj.sim = remApi('remoteApi');
            obj.sim.simxFinish(-1);
            obj.clientID = obj.sim.simxStart('127.0.0.1', port, true, true, 5000, 5);
            if (obj.clientID > -1)
                disp('Connected to simulator');
            else
                disp('Error in connection');
            end
            % enable the synchronous mode on the client:
            obj.sim.simxSynchronous(obj.clientID, true);
            % start the simulation:
            obj.sim.simxStartSimulation(obj.clientID, obj.sim.simx_opmode_blocking);
            
            [returnCode, obj.motor_left] = obj.sim.simxGetObjectHandle(obj.clientID, 'LeftMotor', obj.sim.simx_opmode_blocking);
            [returnCode, obj.motor_right] = obj.sim.simxGetObjectHandle(obj.clientID, 'RightMotor', obj.sim.simx_opmode_blocking);
            [returnCode, obj.joint1] = obj.sim.simxGetObjectHandle(obj.clientID, 'ArmJoint1', obj.sim.simx_opmode_blocking);
            [returnCode, obj.joint2] = obj.sim.simxGetObjectHandle(obj.clientID, 'ArmJoint2', obj.sim.simx_opmode_blocking); 
            [returnCode, obj.base] = obj.sim.simxGetObjectHandle(obj.clientID, 'MobileManipulator', obj.sim.simx_opmode_blocking); 
        
            [returnCode, position] = obj.sim.simxGetObjectPosition(obj.clientID, obj.base, -1, obj.sim.simx_opmode_streaming);
            [returnCode, orientation] = obj.sim.simxGetObjectOrientation(obj.clientID, obj.base, -1, obj.sim.simx_opmode_streaming);
            [returnCode, theta1] = obj.sim.simxGetJointPosition(obj.clientID, obj.joint1, obj.sim.simx_opmode_streaming);
            [returnCode, theta2] = obj.sim.simxGetJointPosition(obj.clientID, obj.joint2, obj.sim.simx_opmode_streaming);
            [returnCode, theta1_d] = obj.sim.simxGetObjectFloatParameter(obj.clientID, obj.joint1, 2012, obj.sim.simx_opmode_streaming);
            [returnCode, theta2_d] = obj.sim.simxGetObjectFloatParameter(obj.clientID, obj.joint2, 2012, obj.sim.simx_opmode_streaming);
            [returnCode, theta_l_d] = obj.sim.simxGetObjectFloatParameter(obj.clientID, obj.motor_left, 2012, obj.sim.simx_opmode_streaming);
            [returnCode, theta_r_d] = obj.sim.simxGetObjectFloatParameter(obj.clientID, obj.motor_right, 2012, obj.sim.simx_opmode_streaming);
            
        end
        
        function Close(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            obj.sim.simxStopSimulation(obj.clientID, obj.sim.simx_opmode_blocking);
            obj.sim.simxFinish(-1);
            obj.sim.delete();
        end
        
        function ApplyControl(obj, u, delta_t)
            display("apply control");
            display(u);
            returnCode = obj.sim.simxSetJointTargetVelocity(obj.clientID, obj.motor_left, sign(u(1)) * 10^10, obj.sim.simx_opmode_oneshot);
            returnCode = obj.sim.simxSetJointTargetVelocity(obj.clientID, obj.motor_right, sign(u(2)) * 10^10, obj.sim.simx_opmode_oneshot);
            returnCode = obj.sim.simxSetJointTargetVelocity(obj.clientID, obj.joint1, sign(u(3)) * 10^10, obj.sim.simx_opmode_oneshot);   
            returnCode = obj.sim.simxSetJointTargetVelocity(obj.clientID, obj.joint2, sign(u(4)) * 10^10, obj.sim.simx_opmode_oneshot);
            
            returnCode = obj.sim.simxSetJointForce(obj.clientID, obj.motor_left, abs(u(1)), obj.sim.simx_opmode_oneshot);
            returnCode = obj.sim.simxSetJointForce(obj.clientID, obj.motor_right, abs(u(2)), obj.sim.simx_opmode_oneshot);
            returnCode = obj.sim.simxSetJointForce(obj.clientID, obj.joint1, abs(u(3)), obj.sim.simx_opmode_oneshot);
            returnCode = obj.sim.simxSetJointForce(obj.clientID, obj.joint2, abs(u(4)), obj.sim.simx_opmode_oneshot);
            
            for i = 1:(delta_t/obj.step_time_vrep)+1
                obj.sim.simxSynchronousTrigger(obj.clientID);
                % To overcome delay in values according to (Remote API modus operandi) document
                ping = obj.sim.simxGetPingTime(obj.clientID);  
            end
            
%             returnCode = obj.sim.simxSetJointTargetVelocity(obj.clientID, obj.motor_left, 0, obj.sim.simx_opmode_oneshot);
%             returnCode = obj.sim.simxSetJointTargetVelocity(obj.clientID, obj.motor_right, 0, obj.sim.simx_opmode_oneshot);
%             returnCode = obj.sim.simxSetJointTargetVelocity(obj.clientID, obj.joint1, 0, obj.sim.simx_opmode_oneshot);   
%             returnCode = obj.sim.simxSetJointTargetVelocity(obj.clientID, obj.joint2, 0, obj.sim.simx_opmode_oneshot);

        end
        
        function state = GetState(obj)
            %q q_d v
            %check if opcode blocking slow down this function
            
            [returnCode, position] = obj.sim.simxGetObjectPosition(obj.clientID, obj.base, -1, obj.sim.simx_opmode_buffer);
            [returnCode, orientation] = obj.sim.simxGetObjectOrientation(obj.clientID, obj.base, -1, obj.sim.simx_opmode_buffer);
            [returnCode, theta1] = obj.sim.simxGetJointPosition(obj.clientID, obj.joint1, obj.sim.simx_opmode_buffer);
            [returnCode, theta2] = obj.sim.simxGetJointPosition(obj.clientID, obj.joint2, obj.sim.simx_opmode_buffer);
            
            rot_matrix = eul2rotm(orientation,'XYZ');
            hom_matrix = eye(4, 4);
            hom_matrix(1:3, 1:3) = rot_matrix;
            hom_matrix(1:3, 4) = position;
            position_pointF = hom_matrix * [obj.l_g, 0, 0, 1]';
            q = double([position_pointF(1), position_pointF(2), orientation(3), theta1, theta2]);
            
            %[returnCode, linear_vel, angular_vel] = obj.sim.simxGetObjectVelocity(obj.clientID, obj.base, obj.sim.simx_opmode_buffer);
            [returnCode, theta1_d] = obj.sim.simxGetObjectFloatParameter(obj.clientID, obj.joint1, 2012, obj.sim.simx_opmode_buffer);
            [returnCode, theta2_d] = obj.sim.simxGetObjectFloatParameter(obj.clientID, obj.joint2, 2012, obj.sim.simx_opmode_buffer);
            %r = position_pointF(1:3) - position';
            %linear_vel_pointF = linear_vel + cross(angular_vel, r);
            %q_d = double([linear_vel_pointF(1), linear_vel_pointF(2), angular_vel(3), theta1_d, theta2_d]);
            
            %[returnCode, theta_l] = obj.sim.simxGetJointPosition(obj.clientID, obj.motor_left, obj.sim.simx_opmode_buffer);
            %[returnCode, theta_r] = obj.sim.simxGetJointPosition(obj.clientID, obj.motor_right, obj.sim.simx_opmode_buffer);
            [returnCode, theta_l_d] = obj.sim.simxGetObjectFloatParameter(obj.clientID, obj.motor_left, 2012, obj.sim.simx_opmode_buffer);
            [returnCode, theta_r_d] = obj.sim.simxGetObjectFloatParameter(obj.clientID, obj.motor_right, 2012, obj.sim.simx_opmode_buffer);
            
            v = double([theta_l_d, theta_r_d, theta1_d, theta2_d]);
            state = [q'; v'];
        end
    end
end

