classdef VrepConnectorNoDym
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = private)
        sim;
        clientID;
        model;
    end
    
    methods
        function obj = VrepConnectorNoDym(model, port)
            %UNTITLED3 Construct an instance of this class
            %   Detailed explanation goes here
            obj.sim = remApi('remoteApi');
            obj.sim.simxFinish(-1);
            obj.clientID = obj.sim.simxStart('127.0.0.1', port, true, true, 5000, 5);
            obj.model = model;
            if (obj.clientID > -1)
                disp('Connected to simulator');
            else
                disp('Error in connection');
            end
        end
        
        function Close(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            obj.sim.simxFinish(-1);
            obj.sim.delete();
        end
        
        function RefreshSim(obj)
            [returnCode, base] = obj.sim.simxGetObjectHandle(obj.clientID, 'MobileManipulator', obj.sim.simx_opmode_blocking);
            [returnCode, joint1] = obj.sim.simxGetObjectHandle(obj.clientID, 'ArmJoint1', obj.sim.simx_opmode_blocking);
            [returnCode, joint2] = obj.sim.simxGetObjectHandle(obj.clientID, 'ArmJoint2', obj.sim.simx_opmode_blocking);        
                
            returnCode = obj.sim.simxSetObjectPosition(obj.clientID, base, -1, [obj.model.q(1), obj.model.q(2), 0.24], obj.sim.simx_opmode_blocking);
            returnCode = obj.sim.simxSetObjectOrientation(obj.clientID, base, -1, [0, 0, obj.model.q(3)], obj.sim.simx_opmode_blocking);

            returnCode = obj.sim.simxSetJointPosition(obj.clientID, joint1, obj.model.q(4), obj.sim.simx_opmode_blocking);
            returnCode = obj.sim.simxSetJointPosition(obj.clientID, joint2, obj.model.q(5), obj.sim.simx_opmode_blocking);

        end
    end
end

