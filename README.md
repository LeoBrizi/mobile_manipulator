# mobile_manipulator

Motion planning for mobile manipulators with sampling-based MPC. For further information see the report.

<!---
# How To Use CoppeliaSim
* install CoppeliaSim from [here](https://www.coppeliarobotics.com/ubuntuVersions).
* add to this workspace the libraries for the communication with CoppeliaSim:
    * remApi.m
    * remoteApi.so
    * remoteApiProto.m
* open the scene which is in the vrep_simulation directory
* start the simulation on CoppeliaSim
* run the scripts for the initializzazion of the parameters
    * dynamic_parameters.m
    * kinematic_parameters.m
* now run the script matlab test.m 

(you can find the libraries for the communication with CoppeliaSim inside the source directories of CoppeliaSim /remoteApiBindings/matlab and /remoteApiBindings/lib/lib)
-->

