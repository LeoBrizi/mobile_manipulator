clear;
clc;

total_input_traj = {};
total_q_traj = {};
total_q_d_traj = {};
total_v_traj = {};
total_EEv_traj = {};
total_time_traj = {};
total_LQR = {};
total_color = {};
total_switch_t = {};
total_switch_it = {};

% no LQR ================================================
SampleMPC;

total_input_traj{1} = input_traj;
total_q_traj{1} = q_traj;
total_q_d_traj{1} = q_d_traj;
total_v_traj{1} = v_traj;
total_EEv_traj{1} = EEv_traj;
total_time_traj{1} = time_traj;
total_LQR{1} = false;
total_color{1} = [0.1986 0.7214 0.6310 1];
total_switch_t{1} = switch_t;
total_switch_it{1} = switch_it;

    
% LQR ====================================================
Sample_MPC_with_LQR_reg;

total_input_traj{2} = input_traj;
total_q_traj{2} = q_traj;
total_q_d_traj{2} = q_d_traj;
total_v_traj{2} = v_traj;
total_EEv_traj{2} = EEv_traj;
total_time_traj{2} = time_traj;
total_LQR{2} = true;
total_color{2} = [0.2081 0.1663 0.5292 1];
total_switch_t{2} = switch_t;
total_switch_it{2} = switch_it;

%%
%%%%%%%%%%%%%%%%%%%%% PLOTS %%%%%%%%%%%%%%%%%%%%%%%
plot_switch=true;

%input
clf;
legStr = {};
for i=size(total_input_traj,2):-1:1
    matrix = cell2mat(total_input_traj{i}(1:800));
    u1 = matrix(1,:);
    u2 = matrix(2,:);
    u3 = matrix(3,:);
    u4 = matrix(4,:);
    x = cell2mat(total_time_traj{i}(1:800));
    x_previous = 0;
    
    subplot(4, 1, 1);
    for j = 1:size(matrix,2)-1
        line([x_previous, x(j)], [u1(j), u1(j)], 'Color', total_color{i}, 'LineWidth', 1, 'HandleVisibility','off');
        line([x(j), x(j)], [u1(j), u1(j+1)], 'Color', total_color{i}, 'LineWidth', 1, 'HandleVisibility','off');
        x_previous = x(j);
    end
    line([x(j), x(j)], [u1(j), u1(j+1)], 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(1,3), -planner.lim(1,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(1,3), planner.lim(1,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1  
        grid minor;
        %title('Input evolutions on wheels and arm joints');
        ylabel('\fontsize{12}{0}$\tau_{l}$ \fontsize{10}{0}$ \textsf{[N$\cdot$m]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(1,3) + planner.lim(1,3)/10) planner.lim(1,3) + planner.lim(1,3)/10]);
        legStr{end+1} = 'GS';
        leg = legend(legStr);
    else
        legStr = {'LQR'};
    end
    x_previous = 0;
    
    subplot(4, 1, 2);
    for j = 1:size(matrix,2)-1
        line([x_previous, x(j)], [u2(j), u2(j)], 'Color', total_color{i}, 'LineWidth', 1, 'HandleVisibility','off');
        line([x(j), x(j)], [u2(j), u2(j+1)], 'Color', total_color{i}, 'LineWidth', 1, 'HandleVisibility','off');
        x_previous = x(j);
    end
    line([x(j), x(j)], [u2(j), u2(j+1)], 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(2,3), -planner.lim(2,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(2,3), planner.lim(2,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1  
        grid minor;
        ylabel('\fontsize{12}{0}$\tau_{r}$ \fontsize{10}{0}$ \textsf{[N$\cdot$m]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(2,3) + planner.lim(2,3)/10) planner.lim(2,3) + planner.lim(2,3)/10]);
        leg = legend(legStr);
    end
    x_previous = 0;
    subplot(4, 1, 3);
    
    for j = 1:size(matrix,2)-1
        line([x_previous, x(j)], [u3(j), u3(j)], 'Color', total_color{i}, 'LineWidth', 1, 'HandleVisibility','off');
        line([x(j), x(j)], [u3(j), u3(j+1)], 'Color', total_color{i}, 'LineWidth', 1, 'HandleVisibility','off');
        x_previous = x(j);
    end
    line([x(j), x(j)], [u3(j), u3(j+1)], 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(3,3), -planner.lim(3,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(3,3), planner.lim(3,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$\tau_{1}$ \fontsize{10}{0}$ \textsf{[N$\cdot$m]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(3,3) + planner.lim(3,3)/10) planner.lim(3,3) + planner.lim(3,3)/10]);
        leg = legend(legStr);
    end
    x_previous = 0;
    
    subplot(4, 1, 4);
    for j = 1:size(matrix,2)-1
        line([x_previous, x(j)], [u4(j), u4(j)], 'Color', total_color{i}, 'LineWidth', 1, 'HandleVisibility','off');
        line([x(j), x(j)], [u4(j), u4(j+1)], 'Color', total_color{i}, 'LineWidth', 1, 'HandleVisibility','off');
        x_previous = x(j);
    end
    line([x(j), x(j)], [u4(j), u4(j+1)], 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(4,3), -planner.lim(4,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(4,3), planner.lim(4,3)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$\tau_{2}$ \fontsize{10}{0}$ \textsf{[N$\cdot$m]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(4,3) + planner.lim(4,3)/10) planner.lim(4,3) + planner.lim(4,3)/10]);
        leg = legend(legStr);
    end
    
end
saveas(gca,'plots/input_comp.png');

%% vector q
clf;
legStr = {};
for i=size(total_q_traj,2):-1:1
    matrix = cell2mat(total_q_traj{i}(1:800));
    q1 = matrix(1,:);
    q2 = matrix(2,:);
    q3 = matrix(3,:);
    q4 = matrix(4,:);
    q5 = matrix(5,:);
    x = cell2mat(total_time_traj{i}(1:800));
    %sgtitle('Evolutions of q components');
    
    subplot(3, 2, 1);
    line(x, q1, 'Color', total_color{i}, 'LineWidth', 1);
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$x_{F}$ \fontsize{10}{0}$ \textsf{[m]}$', 'Interpreter','latex');
        xlabel('Time [s]');
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        legStr{end+1} = 'GS';
        leg = legend(legStr);
        set(leg,'location','best');
    else
        legStr = {'LQR'};
    end
    
    subplot(3, 2, 2);
    line(x, q2, 'Color', total_color{i}, 'LineWidth', 1);
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$y_{F}$ \fontsize{10}{0}$ \textsf{[m]}$', 'Interpreter','latex');
        xlabel('Time [s]');
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        leg = legend(legStr);
        set(leg,'location','best');
    end
    
    subplot(3, 2, 3);
    line(x, q3, 'Color', total_color{i}, 'LineWidth', 1);
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$\phi$ \fontsize{10}{0}$ \textsf{[rad]}$', 'Interpreter','latex');
        xlabel('Time [s]');
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        leg = legend(legStr);
        set(leg,'location','best')
    end
    
    subplot(3, 2, 5);
    line(x, q4, 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(3,1), -planner.lim(3,1)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(3,1), planner.lim(3,1)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$\theta_{1}$ \fontsize{10}{0}$ \textsf{[rad]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(3,1) + planner.lim(3,1)/10) planner.lim(3,1) + planner.lim(3,1)/10]);
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        leg = legend(legStr);
        set(leg,'location','best')
    end
    
    subplot(3, 2, 6);
    line(x, q5, 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(4,1), -planner.lim(4,1)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(4,1), planner.lim(4,1)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$\theta_{2}$ \fontsize{10}{0}$ \textsf{[rad]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(4,1) + planner.lim(4,1)/10) planner.lim(4,1) + planner.lim(4,1)/10]);
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        leg = legend(legStr);
        set(leg,'location','best')
    end
end
saveas(gca,'plots/q_comp.png');

%% vector v
clf;
legStr = {};
for i=size(total_v_traj,2):-1:1
    matrix = cell2mat(total_v_traj{i}(1:800));
    v1 = matrix(1,:);
    v2 = matrix(2,:);
    v3 = matrix(3,:);
    v4 = matrix(4,:);
    x = cell2mat(total_time_traj{i}(1:800));
    subplot(4, 1, 1);
    line(x, v1, 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(1,2), -planner.lim(1,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(1,2), planner.lim(1,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if i==1
        %title('Velocity evolutions on wheels and arm joints');
        grid minor;
        ylabel('\fontsize{12}{0}$\dot{\theta}_{l}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(1,2) + planner.lim(1,2)/10) planner.lim(1,2) + planner.lim(1,2)/10]);
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        legStr{end+1} = 'GS';
        leg = legend(legStr);
    else
        legStr = {'LQR'};
    end
    subplot(4, 1, 2);
    line(x, v2, 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(2,2), -planner.lim(2,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(2,2), planner.lim(2,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$\dot{\theta}_{r}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(2,2) + planner.lim(2,2)/10) planner.lim(2,2) + planner.lim(2,2)/10]);
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        leg = legend(legStr);
    end
    subplot(4, 1, 3);
    line(x, v3, 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(3,2), -planner.lim(3,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(3,2), planner.lim(3,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$\dot{\theta}_{1}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(3,2) + planner.lim(3,2)/10) planner.lim(3,2) + planner.lim(3,2)/10]);
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        leg = legend(legStr);
    end
    subplot(4, 1, 4);
    line(x, v4, 'Color', total_color{i}, 'LineWidth', 1);
    line([0, x(end)], [-planner.lim(4,2), -planner.lim(4,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    line([0, x(end)], [planner.lim(4,2), planner.lim(4,2)], 'LineWidth', 0.05, 'Color','red', 'LineStyle','--', 'HandleVisibility','off');
    if i==1
        grid minor;
        ylabel('\fontsize{12}{0}$\dot{\theta}_{2}$ \fontsize{10}{0}$ \textsf{[rad /s]}$', 'Interpreter','latex');
        xlabel('Time [s]');
        ylim([-(planner.lim(4,2) + planner.lim(4,2)/10) planner.lim(4,2) + planner.lim(4,2)/10]);
    end
    if plot_switch
        if total_LQR{i}, xline(total_switch_t{i}, ':', {'Controller','Switch'}, 'FontSize', 9, 'LabelVerticalAlignment', 'bottom', 'LineWidth', 1, 'HandleVisibility','off');
        else, xline(total_switch_t{i}, ':', {'Sampling','Switch'}, 'FontSize', 9, 'LineWidth', 1, 'HandleVisibility','off'); end
    end
    if i==1
        leg = legend(legStr);
    end
end
saveas(gca,'plots/velocity_v_comp.png');
