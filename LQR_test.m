addpath dynamic_model/;
addpath regulation/;

kinematic_parameters;
dynamic_parameters;

model = DynamicModel();

model.Step([0.1;0.1;0;0],0.2);
model.Step([0.1;0.1;0;0],0.2);
model.Step([0.1;0.1;0;0],0.2);
model.Step([0.1;0.1;0;0],0.2);
model.Step([-0.1;-0.1;0;0],0.2);
model.Step([-0.1;-0.1;0;0],0.2);
model.Step([-0.1;-0.1;0;0],0.2);

display("initial state");
[model.q;model.v]
display("direct kin");
model.DirectKinematic(model.q)

goal = model.InverseKinematicEE([0.67;0],0.1,1e-5)

goal = [goal;0;0;0;0];

K = regularize(model, [-0.1;-0.1;0;0]);
-K*([model.q;model.v]-goal)
for i=1:100
    model.Step(-K*([model.q;model.v]-goal),0.05);
end

    
    